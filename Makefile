# vim: set ft=make:

MAKEFLAGS += --jobs=2
PLAT ?= unix
# Set to the path to stm32f7-game checkout
BASE_DIR := lib
GAME_DIR := stealthy
UNIFIED_BUILD := 1

GAMEDEFS := -DDEFAULT_PIXEL_SCALE=3

GAME_PALETTE := $(GAME_DIR)/stealthy.gpl

ASSETTABLE = $(GAME_DIR)/asset_table.h

SRCS := \
    $(GAME_DIR)/main.cc \

# Asset names require frame size in col x row
GAME_ASSETS := \
    $(GAME_DIR)/map.dat \
	$(GAME_DIR)/level1.dat \
	$(GAME_DIR)/level2.dat \
    $(GAME_DIR)/level3.dat \
    $(GAME_DIR)/tiles_v2_8x8.dat \
    $(GAME_DIR)/map_level_1.dat \
    $(GAME_DIR)/shading.dat \
    $(GAME_DIR)/player_10x10.dat \
    $(GAME_DIR)/scanner_8x8.dat \
    $(GAME_DIR)/light_8x8.dat \
	$(GAME_DIR)/shooter_8x8.dat \
	$(GAME_DIR)/shot_5x5.dat \
    $(GAME_DIR)/legEnemyWalk_13x18.dat \
    $(GAME_DIR)/legEnemyStand_12x18.dat \
	$(GAME_DIR)/legEnemyShoot_13x18.dat \
	$(GAME_DIR)/legEnemyShot_43x18.dat \
    $(GAME_DIR)/door_13x16.dat \
    $(GAME_DIR)/sidedoor_8x24.dat \
    $(GAME_DIR)/panel_16x12.dat \
    $(GAME_DIR)/hackbar_12x4.dat \
    $(GAME_DIR)/menupeek_9x9.dat \
    $(GAME_DIR)/menuhack_9x9.dat \
    $(GAME_DIR)/menuenter_9x9.dat \
    $(GAME_DIR)/menu_64x64.dat \
    $(GAME_DIR)/menuitems_48x9.dat \
    $(GAME_DIR)/file_11x9.dat \
    $(GAME_DIR)/splash_64x64.dat \
	$(GAME_DIR)/logo_54x18.dat \
    $(GAME_DIR)/exit_12x24.dat \
    $(GAME_DIR)/hud_4x4.dat \
    $(GAME_DIR)/alert_3x5.dat \
    $(GAME_DIR)/switch_8x8.dat \
    $(GAME_DIR)/downarrow_6x6.dat \
	$(GAME_DIR)/snd_stealth_escape.dat \
	$(GAME_DIR)/snd_retro_footstep_03_jump.dat \
	$(GAME_DIR)/snd_retro_impact_punch_07_hit.dat \
	$(GAME_DIR)/snd_retro_footstep_mud_01_land.dat \
	$(GAME_DIR)/snd_retro_electric_sparks_01_hack.dat \
	$(GAME_DIR)/snd_retro_weapon_electric_18_shoot.dat \
	$(GAME_DIR)/snd_retro_event_stereoup_02_confirm.dat \
	$(GAME_DIR)/snd_retro_event_acute_11_menu.dat \
	$(GAME_DIR)/snd_retro_event_49_win.dat \
	$(GAME_DIR)/snd_retro_charge_off_stereoup_05_door.dat \
	$(GAME_DIR)/credits_64x64.dat \
	$(GAME_DIR)/back_38x9.dat \
	$(GAME_DIR)/winscreen_64x64.dat \
	$(GAME_DIR)/losescreen_64x64.dat \

include $(BASE_DIR)/src/plat_$(PLAT)/defs.mk
include $(BASE_DIR)/common.mk

serve: $(BINDIR)/gameloader.js $(BINDIR)/index.html
	cd $(BINDIR); python -m SimpleHTTPServer
