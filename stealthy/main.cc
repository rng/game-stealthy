#include <math.h>
#include <string.h>

#include "engine/engine.h"

#define TILE_SIZE_PX (8)
#define DEBUG_BOUNDS (0)
#define INF_LIVES (0)

#ifndef M_PI
#define M_PI (3.1415926535f)
#endif

#include "game.h"
#include "lighting.h"

#include "alertmgr.h"

char LEVEL_FILE_FORMAT[] = "level%d.dat";

enum {
    SCENE_SPLASH = 0,
    SCENE_MENU,
    SCENE_MAIN,
    SCENE_CREDITS,
    SCENE_LEVELEND,
    SCENE_DIE,
    //
    SND_VOLUME_SFX = 128,
    SND_VOLUME_BG = 80,
    SND_VOLUME_EXPLODE = 60,
    //
    MAX_LEVEL = 3,
};

struct hud_anim_t {
    int frame_count, frame;
    float t;
};

struct game_particles_t;

typedef struct game_state_t {
    v2i screen_size;
    uint8_t screen[GAME_RES_WIDTH * GAME_RES_HEIGHT];
    platform_params_t plat_params;
    tilemap_t map;
    scenes_t scenes;
    entitylist_t entlist;
    entitymgr_t entitymgr;
    entity_t *player;
    uint8_t level;

    rect_t view;
    lighting_t lighting;
    collide_conf_t collide;
    render_t render;
    sound_mixer_t mixer;
    camera_t camera;
    v2f camera_target; // for camera tracking, does not offset the pos by view_size/2
    alertmgr_t alertmgr;
    anim_t splash_scene_anim;

    float time;
    struct {
        hud_anim_t anims[4];
        bool visible;
    } hud;

    struct {
        bool active;
        v2f offset;
    } peekview;

    struct {
        bool started;
        bool quit;
        int pos;
    } menustate;

    game_particles_t *particles;
    int bg_music_channel;
    bool is_switching;
} game_state_t;

#include "particle.h"
#include "spark.h"

struct game_particles_t {
    emitter_t<spark_t, 32> sparks;
};

#include "entities.h"

#include "enemies.h"

platform_api_t *platform;

static const entity_type_t *const entity_types[] = {
    &player_type, &scanner_type,     &light_type,  &laser_type,   &enemy_type,    &waypoint_type,
    &door_type,   &panel_type,       &exit_type,   &trigger_type, &sidedoor_type, &shooter_type,
    &mover_type,  &timetrigger_type, &switch_type, &grate_type,   &goal_type,     &enemyshot_type,
};

const int ladders[] = {116, 117, 95, 73, 51}; // tiles indexed by 1
const int oneways[] = {3, 4, 5, 26};

static bool tilelist_contains(int t, const int *l, size_t n) {
    for (size_t i = 0; i < n; i++) {
        if (l[i] == t) {
            return true;
        }
    }
    return false;
}
#define TILELIST_CONTAINS(T, L) tilelist_contains(T, L, NELEMS(L))

int map_tile(void *context, int x, int y) {
    tilemap_t *map = (tilemap_t *)context;
    if (tilemap_inside(map, x, y)) {
        // are we on a ladder (check against ladder tiles from our tileset)?
        int decor_tile = tilemap_tile(map, 2, x, y);
        if (TILELIST_CONTAINS(decor_tile, ladders)) {
            return COLLIDE_TILE_LADDER;
        }
        if (TILELIST_CONTAINS(decor_tile, oneways)) {
            return COLLIDE_TILE_ONEWAY;
        }
        // otherwise can we walk through this tile
        if (tilemap_tile(map, 2, x, y) == 0) {
            return COLLIDE_TILE_PASSABLE;
        }
    }
    return COLLIDE_TILE_IMPASSABLE;
}

void hud_render(game_state_t *game, input_state_t *input, bitmap_t *screen) {
    auto player = (player_t *)entitymgr_find_by_flag(&game->entitymgr, ENTITY_FLAG_PLAYER, NULL);
    // draw healthbar
    for (int i = 0; i < 3; i++) {
        render_push_sprite_s(&game->render, R_ID_HUD, v2f(i * 4 + 1, 1), Z_ENT_PLAYER, false,
                             player->health > i ? 1 : 0);
    }
    // draw visibility indicator
    v2f epos = player->e.pos + v2f(4, 4);
    bool visible = point_is_visible(game, epos);
    render_push_sprite_s(&game->render, R_ID_HUD, v2f(16, 1), Z_ENT_PLAYER, false, visible ? 3 : 2);

    // draw interactable object hover items
    interactive_entity_t *ent = (interactive_entity_t *)player->hovered_ent;
    if (ent && !ent->hover_hidden) {
        interaction_t *hover = (interaction_t *)ent->type->extra;
        int i = 0;
        for (; hover->asset != -1; hover++) {
            hud_anim_t *anim = &game->hud.anims[i];
            // initialise animation for hover item if we've just entered a new interactable
            if (!game->hud.visible) {
                anim->frame_count = hover->anim_frames;
                anim->frame = 0;
                anim->t = 0;
            }
            anim->t += input->dt;
            if (anim->t > 0.3f) {
                anim->t = 0;
                anim->frame = (anim->frame + 1) % anim->frame_count;
            }
            // draw the hover item sprite
            v2f pos = player->hovered_ent->pos + v2f(-2 + i * 10, -10);
            render_push_sprite_w(&game->render, hover->asset, pos, Z_ENT_PLAYER, false,
                                 anim->frame);
            i++;
        }
        game->hud.visible = true;
    } else {
        game->hud.visible = false;
    }
}

void game_render_map(game_state_t *game) {
    render_push_tiles_parallax_w(&game->render, &game->map, 0, Z_MAP_PARALLAX, v2f(-10., 50.));
    render_push_tiles_w(&game->render, &game->map, 1, Z_MAP_BG1);
    render_push_tiles_w(&game->render, &game->map, 2, Z_MAP_BG2);
    render_push_tiles_w(&game->render, &game->map, 3, Z_MAP_BASE);
    render_push_tiles_w(&game->render, &game->map, 4, Z_MAP_FG);
}

v2f calc_cam_targ(game_state_t *game, float dt) {
    player_t *player = (player_t *)game->player;
    v2f res = game->camera_target;

    // The camera window is rectangle around the camera target. We try to keep the player within
    // this area.
    const float cam_window_lrd = 8.0f;
    const float cam_window_up = 18.0f;
    bool inside_window = (game->player->pos.x > (res.x - cam_window_lrd)) &&
                         (game->player->pos.x < (res.x + cam_window_lrd)) &&
                         (game->player->pos.y > (res.y - cam_window_up)) &&
                         (game->player->pos.y < (res.y + cam_window_lrd));

    // Look
    if (player->camofs.y != 0.0f) {
        v2f final_camtarg = game->player->pos + player->camofs;
        final_camtarg.x = game->camera_target.x;

        // Snap to the final position if the target is close enough
        if (res.dist(final_camtarg) < 0.1f) {
            res = final_camtarg;
        } else {
            res = res.lerp(final_camtarg, 0.6f);
        }
        game->camera_target = res;
        return res;
    }

    if (!inside_window) {
        // If the player is moving and outside the camera window, move the
        // camera window to cover the player
        if ((player->vel.x != 0.0f) || (player->vel.y != 0.0f)) {
            if (game->player->pos.x < (res.x - cam_window_lrd)) {
                // Outside left edge
                float a = res.x - cam_window_lrd - game->player->pos.x;
                res.x -= a;
            } else if (game->player->pos.x > (res.x + cam_window_lrd)) {
                // Outside right edge
                float a = game->player->pos.x - (res.x + cam_window_lrd);
                res.x += a;
            }

            if (game->player->pos.y < (res.y - cam_window_up)) {
                // Outside top edge
                float a = res.y - cam_window_up - game->player->pos.y;
                res.y -= a;
            } else if (game->player->pos.y > (res.y + cam_window_lrd)) {
                // Outside bottom edge
                float a = game->player->pos.y - (res.y + cam_window_lrd);
                res.y += a;
            }

        } else {
            // Lerp the camera target to the player
            v2f final_camtarg = game->player->pos;
            // Snap to the final position if the target is close enough
            if (res.dist(final_camtarg) < 0.1f) {
                res = final_camtarg;
            } else {
                res = res.lerp(final_camtarg, 0.2f);
            }
        }
    }

    game->camera_target = res;
    return res;
}

#define SPLASH_SCENE_NUM_FRAMES 16
static const uint16_t _splash_scene_frames[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

void scene_splash(void *ctx, input_state_t *input, bitmap_t *screen, bool first) {
    game_state_t *game = (game_state_t *)ctx;

    // Splash art
    render_push_sprite_s(&game->render, R_ID_SPLASH, {0, 0}, Z_ENT_UI, false, 0);

    // Logo animation
    anim_update(&game->splash_scene_anim, input->dt);
    render_push_sprite_s(&game->render, R_ID_LOGO, {5, 44}, Z_ENT_UI, false,
                         game->splash_scene_anim.frame);

    render_draw(&game->render, screen, &game->view);

    if (input->clicked(BUTTON_A_BIT) || input->clicked(BUTTON_B_BIT) ||
        input->clicked(BUTTON_C_BIT)) {
        scene_switch(&game->scenes, SCENE_MENU, transition_pixel, 0.5, true);
    }
}

void scene_credits(void *ctx, input_state_t *input, bitmap_t *screen, bool first) {
    game_state_t *game = (game_state_t *)ctx;
    bitmap_fill(screen, 0);

    render_push_sprite_s(&game->render, R_ID_CREDITS, {0, 0}, Z_ENT_BG3, false, 0);

    render_push_sprite_s(&game->render, R_ID_BACK, {13, 49}, Z_ENT_UI, false, 0);

    if (input->clicked(BUTTON_A_BIT) || input->clicked(BUTTON_B_BIT) ||
        input->clicked(BUTTON_C_BIT)) {
        sound_play_note(&game->mixer, S_ID_CONFIRM, SOUND_NOTE_C4, SND_VOLUME_SFX);

        scene_switch(&game->scenes, SCENE_MENU, transition_pixel, 0.5, true);
    }
    render_draw(&game->render, screen, &game->view);
}

void scene_menu(void *ctx, input_state_t *input, bitmap_t *screen, bool first) {
    game_state_t *game = (game_state_t *)ctx;
    bitmap_fill(screen, 0);

    render_push_sprite_s(&game->render, R_ID_MENU, {0, 0}, Z_ENT_BG3, false, 0);

    auto menuitem = [game](int i, float y, int which) {
        int f = (i == game->menustate.pos) ? which + 1 : which;
        render_push_sprite_s(&game->render, R_ID_MENUITEMS, {8, y}, Z_ENT_UI, false, f);
    };

    menuitem(0, 27, game->menustate.started ? 2 : 0);
    menuitem(1, 39, 4);
    menuitem(2, 51, 6);

    if (input->clicked(BUTTON_UP_BIT)) {
        if (game->menustate.pos > 0) {
            sound_play_note(&game->mixer, S_ID_MENU, SOUND_NOTE_C4, SND_VOLUME_SFX);
            game->menustate.pos--;
        }
    }

    if (input->clicked(BUTTON_DOWN_BIT)) {
        if (game->menustate.pos < 2) {
            sound_play_note(&game->mixer, S_ID_MENU, SOUND_NOTE_C4, SND_VOLUME_SFX);
            game->menustate.pos++;
        }
    }

    if (input->clicked(BUTTON_A_BIT) || input->clicked(BUTTON_B_BIT) ||
        input->clicked(BUTTON_C_BIT)) {
        sound_play_note(&game->mixer, S_ID_CONFIRM, SOUND_NOTE_C4, SND_VOLUME_SFX);

        switch (game->menustate.pos) {
            case 0:
                scene_switch(&game->scenes, SCENE_MAIN, transition_pixel, 0.5, true);
                break;
            case 1:
                scene_switch(&game->scenes, SCENE_CREDITS, transition_pixel, 0.5, true);
                break;
            case 2:
                game->menustate.quit = true;
                break;
        }
    }
    render_draw(&game->render, screen, &game->view);
}

void scene_main(void *ctx, input_state_t *input, bitmap_t *screen, bool first) {
    game_state_t *game = (game_state_t *)ctx;
    game->time += input->dt;
    game->is_switching = false;

    bitmap_fill(screen, 1);

    rect_t l_view = game->view.expand2(20, 20);
    light_clear(game->lighting, game->collide, l_view);
    // update entities
    entitymgr_tick(&game->entitymgr, input, game);
    // update game particles
    game->particles->sparks.tick(input->dt, ctx);
    // push map to renderer
    game_render_map(game);
    // update camera
    v2f camtarg = calc_cam_targ(game, input->dt);
    game->view = camera_update(game->camera, camtarg, input->dt);
    // calulate lighting
    light_view(game->lighting, game->view);
    // draw
    render_draw(&game->render, screen, &game->view);

    // draw peek view if needed
    if (game->peekview.active) {
        rect_t orig_view = game->view;
        v2f ofs = game->peekview.offset;

        rect_t bounds = orig_view.translate(ofs.x, ofs.y).expand2(8, 8);
        FOREACH_ENTITY_IN_BOUNDS(e, &game->entitymgr, bounds, ENTITY_FLAG_ALERTABLE) {
            // draw ememies
            render_push_rect_w(&game->render, e->pos.x - ofs.x, e->pos.y - ofs.y,
                               e->pos.x + e->bounds.w - ofs.x, e->pos.y + e->bounds.h - ofs.y,
                               Z_MAP_FG, 7);

            if (1) {
                // draw vision cones
                tri_t cone;
                if (e->type == &scanner_type) {
                    cone = scanner_get_vision_cone((scanner_t *)e);
                } else if (e->type == &enemy_type) {
                    cone = enemy_get_vision_cone((enemy_t *)e);
                } else {
                    continue;
                }
                render_push_line_w(&game->render, cone.a.x - ofs.x, cone.a.y - ofs.y,
                                   cone.b.x - ofs.x, cone.b.y - ofs.y, Z_MAP_FG, 7);
                render_push_line_w(&game->render, cone.b.x - ofs.x, cone.b.y - ofs.y,
                                   cone.c.x - ofs.x, cone.c.y - ofs.y, Z_MAP_FG, 7);
                render_push_line_w(&game->render, cone.c.x - ofs.x, cone.c.y - ofs.y,
                                   cone.a.x - ofs.x, cone.a.y - ofs.y, Z_MAP_FG, 7);
            }
        }

        render_draw(&game->render, screen, &game->view);
    }

    // render lighting
    if (1) {
        DEBUG_TIMED_BLOCK("lighting");
        bitmap_t lightpal = game->render.sprites[R_ID_SHADING];
        light_render(game->lighting, *screen, game->view, lightpal, game->player->pos,
                     game->collide);
    }
    // update alertness status
    game->alertmgr.tick(game->render, game->view);
    // render hud
    hud_render(game, input, screen);
    render_draw(&game->render, screen, &game->view);

    // entity bounding boxes
    if (DEBUG_BOUNDS) {
        entity_t *e = game->entitymgr.entities;
        while (e) {
            if (game->view.inside(e->pos.x, e->pos.y)) {
                v2f pos = e->pos - v2f(game->view.x, game->view.y) + v2f(e->bounds.x, e->bounds.y);
                bitmap_draw_rect(screen, pos.x, pos.y, pos.x + e->bounds.w, pos.y + e->bounds.h,
                                 14);
            }
            e = e->next;
        }
    }

    if (input->clicked(BUTTON_MENU_BIT)) {
        game->menustate.started = true;
        scene_switch(&game->scenes, SCENE_MENU, transition_pixel, 0.5, true);
    }
}

void game_reset(game_state_t *game) {
    game->alertmgr.reset();
    entitymgr_reset(&game->entitymgr);
    entitymgr_spawnlist(&game->entitymgr, &game->entlist);

    entity_t *e = game->entitymgr.entities;
    while (e) {
        if (e->type == &player_type) {
            game->player = e;
            break;
        }
        e = e->next;
    }
    ASSERT(game->player);

    v2f half_screen_size = v2f(game->screen_size.x / 2, game->screen_size.y / 2);
    camera_set_pos(game->camera, game->player->pos - half_screen_size);

    game->time = 0;

    // Move camera to be a tiny bit above the player.
    game->camera.pos = game->player->pos - v2f(0, 8);

    // Background music
    sound_stop_channel(&game->mixer, game->bg_music_channel);
    game->bg_music_channel =
        sound_play_note(&game->mixer, S_ID_ESCAPE, SOUND_NOTE_C4, SND_VOLUME_BG);
}

void load_level(game_state_t *game) {
    char level_file_name[32] = {0};
    snprintf(level_file_name, sizeof(level_file_name), LEVEL_FILE_FORMAT, game->level);
    asset_load_tilemap(level_file_name, &game->map, &game->entlist);

    game->map.tileset = game->render.sprites[R_ID_TILES];

    palette_t pal;
    asset_load_palette("stealthy.dat", &pal);
    platform->gfx_set_pallete((uint8_t *)pal.colours, pal.count);
    asset_free_palette(&pal);

    v2i world_size =
        v2i(game->map.width * game->map.tilesize, game->map.height * game->map.tilesize);
    camera_init(game->camera, game->screen_size, world_size, 50);

    game->collide = (collide_conf_t){
        game->map.tilesize,
        map_tile,
        &game->map,
    };
}

void scene_levelend(void *ctx, input_state_t *input, bitmap_t *screen, bool first) {
    game_state_t *game = (game_state_t *)ctx;

    if (game->is_switching) {
        return;
    }

    if (game->level == MAX_LEVEL) {
        render_push_sprite_s(&game->render, R_ID_WINSCREEN, {0, 0}, Z_ENT_BG3, false, 0);
        render_draw(&game->render, screen, &game->view);
    } else {
        bitmap_fill(screen, 8);
        game->level++;
        load_level(game);
        game_reset(game);
        scene_switch(&game->scenes, SCENE_MAIN, transition_pixel, 0.5, true);
        game->is_switching = true;
    }
}

void scene_die(void *ctx, input_state_t *input, bitmap_t *screen, bool first) {
    game_state_t *game = (game_state_t *)ctx;

    render_push_sprite_s(&game->render, R_ID_LOSESCREEN, {0, 0}, Z_ENT_BG3, false, 0);
    render_draw(&game->render, screen, &game->view);

    if (game->is_switching) {
        return;
    }

    if (input->clicked(BUTTON_A_BIT) || input->clicked(BUTTON_B_BIT) ||
        input->clicked(BUTTON_C_BIT)) {
        load_level(game);
        game_reset(game);
        scene_switch(&game->scenes, SCENE_MAIN, transition_pixel, 0.5, true);
        game->is_switching = true;
    }
}

struct game_state_t *game_init(platform_api_t *platform_api, platform_params_t *params) {
    platform = platform_api;
    struct game_state_t *game = (game_state_t *)platform->alloc(sizeof(game_state_t));
    game->plat_params = *params;

    game->screen_size = v2i(GAME_RES_WIDTH, GAME_RES_HEIGHT);

    sound_init(&game->mixer, params->audio_rate, S_ID_MAX, SOUND_DEFAULT_CHANNELS);
    render_init(&game->render, R_ID_MAX);
    entitymgr_register_types(&game->entitymgr, entity_types, NELEMS(entity_types));

    for (size_t i = 0; i < NELEMS(asset_map); i++) {
        render_load_sprite(&game->render, asset_map[i].id, asset_map[i].filename);
    }
    for (size_t i = 0; i < NELEMS(sound_map); i++) {
        sound_instrument_pcm(&game->mixer, sound_map[i].id, sound_map[i].filename);
    }

    game->level = 1;
    load_level(game);

    game->particles = (game_particles_t *)platform->alloc(sizeof(game_particles_t));

    // Splash Screen
    anim_init_sequence(&game->splash_scene_anim,     // anim_t *self
                       0.025f,                       // float frametime
                       _splash_scene_frames,         // const uint16_t *frames
                       NELEMS(_splash_scene_frames), // uint32_t framecount
                       false);                       // bool loop

    static const scene_func_t scene_fns[] = {
        scene_splash, scene_menu, scene_main, scene_credits, scene_levelend, scene_die,
    };
    scene_init(&game->scenes, scene_fns, NELEMS(scene_fns), SCENE_SPLASH);

    game_reset(game);
    return game;
}

void game_term(game_state_t *game) {
    entitymgr_reset(&game->entitymgr);
    render_term(&game->render);
    sound_term(&game->mixer);
    platform->free(game);
}

bool game_tick(game_state_t *game, input_state_t *input, uint8_t *framebuffer) {
    bitmap_t fb = BITMAP8(game->plat_params.screen_w, game->plat_params.screen_h, framebuffer);
    bitmap_t screen = BITMAP8(game->screen_size.x, game->screen_size.y, game->screen);
    scene_tick(&game->scenes, game, input, &screen);
    bitmap_scale(&fb, &screen, 2);
    return game->menustate.quit;
}

void game_audio(game_state_t *game, void *audio_buf, size_t audio_buf_len) {
    sound_mix(&game->mixer, audio_buf, audio_buf_len);
}
