#include "vision.h"

/*
states
    idle/patrol    [seen] -> alerted
    alerted        [seentime] -> alarm, [notseen] -> lost
    alarm/chase    [notseen] -> lost
    attack         [timeout] -> alarm/chase
    lost           [seen] -> alarm, [notseen] -> idle
*/

enum enemy_ai_state_t {
    ENEMY_AI_STATE_IDLE = 0,
    ENEMY_AI_STATE_ALERT,
    ENEMY_AI_STATE_CHASE,
    ENEMY_AI_STATE_ATTACK,
    ENEMY_AI_STATE_LOST,
};

typedef void (*ai_callback_t)(void *ctx, float dt, game_state_t *game);

// configuration for AI behaviour
struct ai_config_t {
    float vision_width;   // width in radians of vision cone
    int vision_length;    // length of vision cone (distance the enemy can see) in pixels
    bool vision_a_limits; // true if we should impose [min|max]_a limits on our vision angle
    float min_a, max_a;   // min and max angle in radians the enemy can look (0 facing to the right)
    float alert_rate;     // how quickly the enemy "alerts" (notices the player)
    float dealert_rate;   // how quickly the enemy "dealerts" (goes from alerted to idle)
    float track_speed;    // how quickly the vision cone tracks the player
    int alert_range;      // how far away (in pixels) this enemy will alert other enemies
    int attack_range;     // how far away (in pixels) we can attack the player
    float reload_time;    // how quickly the enemy can attack
    // custom behaviours for each state
    ai_callback_t idle_behaviour, alert_behaviour, chase_behaviour, attack_behaviour,
        lost_behaviour;
};

struct enemy_ai_t {
    ai_config_t config;
    enemy_ai_state_t state;
    float facing_a;    // current direction we're facing
    float seen_time;   // how long we've seen the player for (<1: "alerted", >=1: "chasing")
    v2f alert_pos;     // where we (or another enemy) last saw a player
    bool alert_other;  // if we've been alerted by some other enemy
    float alert_time;  // time we've been alerted for
    bool alert_sent;   // have we sent an alert
    float attack_time; // time since last attack

    tri_t get_vision_tri(v2f pos) {
        tri_t tri;
        float vs = (config.vision_width - RADS(2)) / 2;
        tri.a = pos;
        tri.b = tri.a + v2f(cos(facing_a - vs) * config.vision_length,
                            sin(facing_a - vs) * config.vision_length);
        tri.c = tri.a + v2f(cos(facing_a + vs) * config.vision_length,
                            sin(facing_a + vs) * config.vision_length);
        return tri;
    }

    entity_t *check_vision(v2f pos, game_state_t *game) {
        vision_cone_t cone = {
            .center = pos,
            .angle = facing_a,
            .width = config.vision_width,
            .length = config.vision_length,
        };

        switch (state) {
            case ENEMY_AI_STATE_IDLE:
                vision_cone_draw_light(cone, game->lighting, VISCONE_IDLE, 0);
                break;
            case ENEMY_AI_STATE_CHASE:
                vision_cone_draw_light(cone, game->lighting, VISCONE_ALARM, 1.0);
                break;
            case ENEMY_AI_STATE_ALERT:
            case ENEMY_AI_STATE_ATTACK:
            case ENEMY_AI_STATE_LOST:
                vision_cone_draw_light(cone, game->lighting, VISCONE_ALERT, seen_time);
                break;
        }
        return vision_cone_get_visible_entity(&cone, game);
    }

    float limit_angle(float a) {
        return config.vision_a_limits ? CLAMP(a, config.min_a, config.max_a) : a;
    }

    void face_target(v2f pos, v2f target_pos, game_state_t *game, float dt) {
        v2f delta = target_pos - pos;
        float ta = atan2f(delta.y, delta.x);
        float target_a = limit_angle(ta);
        float err = target_a - facing_a;
        if (err > (float)M_PI) {
            err = 2 * (float)M_PI - err;
        } else if (err < (float)-M_PI) {
            err = 2 * (float)M_PI + err;
        }
        float cmd = err * config.track_speed * dt;
        facing_a = limit_angle(facing_a + cmd);
    }

    void force_face_target(v2f pos, v2f target_pos) {
        v2f delta = target_pos - pos;
        float ta = atan2f(delta.y, delta.x);
        bool in_vision = true;
        if (config.vision_a_limits) {
            in_vision = (ta >= config.min_a) && (ta <= config.max_a);
        }
        if (in_vision && (delta.len() <= config.vision_length)) {
            facing_a = limit_angle(ta);
        }
    }

    void change_state(game_state_t *game, entity_t *entity, enemy_ai_state_t newstate) {
        if (newstate == state) {
            return;
        }

        if (newstate == ENEMY_AI_STATE_IDLE) {
            game->alertmgr.dealert(entity);
        } else if (newstate == ENEMY_AI_STATE_ALERT) {
            game->alertmgr.alert(entity);
        }

        state = newstate;
    }

    void alert_from_other(game_state_t *game, entity_t *entity, v2f pos) {
        change_state(game, entity, ENEMY_AI_STATE_ALERT);
        alert_pos = pos;
        alert_other = true;
        alert_time = 5; // timeout if we haven't found the player in 5 sec
    }

    void tick(v2f pos, game_state_t *game, float dt, entity_t *ctx) {
        entity_t *player = check_vision(pos, game);

        switch (state) {
            case ENEMY_AI_STATE_IDLE:
                if (config.idle_behaviour) {
                    config.idle_behaviour(ctx, dt, game);
                }
                if (player) {
                    change_state(game, ctx, ENEMY_AI_STATE_ALERT);
                }
                break;
            case ENEMY_AI_STATE_ALERT:
                if (config.alert_behaviour) {
                    config.alert_behaviour(ctx, dt, game);
                }
                if (player) {
                    // we've found the player, clear alert other state
                    alert_other = false;
                    // we're locked onto the player, track them
                    face_target(pos, entity_center(player), game, dt);
                    seen_time += dt * config.alert_rate;
                    seen_time = MIN(seen_time, 1.f);
                    if (seen_time >= 1) {
                        change_state(game, ctx, ENEMY_AI_STATE_CHASE);
                    }
                } else if (alert_other) {
                    face_target(pos, alert_pos, game, dt);
                    alert_time -= dt;
                    // timeout if we haven't found the player
                    if (alert_time <= 0) {
                        alert_other = false;
                        change_state(game, ctx, ENEMY_AI_STATE_LOST);
                    }
                } else {
                    change_state(game, ctx, ENEMY_AI_STATE_LOST);
                }
                break;
            case ENEMY_AI_STATE_CHASE:
                if (config.chase_behaviour) {
                    config.chase_behaviour(ctx, dt, game);
                }
                if (player) {
                    // we're locked onto the player, chase!
                    if (!alert_sent) {
                        int r = config.alert_range;
                        rect_t bounds = rect_t(pos.x - r, pos.y - r, r * 2, r * 2);
                        FOREACH_ENTITY_IN_BOUNDS(e, &game->entitymgr, bounds,
                                                 ENTITY_FLAG_ALERTABLE) {
                            entity_send_event(e, EVENT_ALERT, player, game);
                        }
                        alert_sent = true;
                    }
                    v2f dist_to_player = entity_center(player) - pos;
                    if (dist_to_player.len() <= config.attack_range) {
                        attack_time = config.reload_time;
                        change_state(game, ctx, ENEMY_AI_STATE_ATTACK);
                    }
                    // face the player
                    face_target(pos, entity_center(player), game, dt);
                } else {
                    alert_sent = false;
                    change_state(game, ctx, ENEMY_AI_STATE_LOST);
                }
                break;
            case ENEMY_AI_STATE_ATTACK:
                if (config.attack_behaviour) {
                    config.attack_behaviour(ctx, dt, game);
                }
                if (player) {
                    face_target(pos, entity_center(player), game, dt);
                }
                attack_time -= dt;
                if (attack_time <= 0) {
                    change_state(game, ctx, ENEMY_AI_STATE_CHASE);
                }
                break;
            case ENEMY_AI_STATE_LOST:
                if (config.chase_behaviour) {
                    config.chase_behaviour(ctx, dt, game);
                }
                if (player) {
                    change_state(game, ctx, ENEMY_AI_STATE_ALERT);
                } else {
                    seen_time -= dt * config.dealert_rate;
                    seen_time = MAX(seen_time, 0.f);
                    if (seen_time <= 0) {
                        change_state(game, ctx, ENEMY_AI_STATE_IDLE);
                    }
                }
                break;
        }
    }
};
