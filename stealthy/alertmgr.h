#define MAX_ALERTED (16)

struct alertmgr_t {
    entity_t *alerted[MAX_ALERTED] = {};

    void alert(entity_t *ent) {
        for (int i = 0; i < MAX_ALERTED; i++) {
            if (alerted[i] == ent) {
                return;
            }
        }
        for (int i = 0; i < MAX_ALERTED; i++) {
            if (!alerted[i]) {
                alerted[i] = ent;
                return;
            }
        }
        ASSERT(!"no space in alerted");
    }

    void dealert(entity_t *ent) {
        for (int i = 0; i < MAX_ALERTED; i++) {
            if (alerted[i] == ent) {
                alerted[i] = NULL;
            }
        }
    }

    void reset() {
        for (int i = 0; i < MAX_ALERTED; i++) {
            alerted[i] = NULL;
        }
    }

    void tick(render_t &render, rect_t &view) {
        rect_t bounds = view.shrink2(2, 3);
        for (int i = 0; i < MAX_ALERTED; i++) {
            entity_t *ent = alerted[i];
            if (ent) {
                v2f p = ent->pos + v2f(2, -6);
                rect_t iconbounds(p.x, p.y, 3, 5);

                if (!bounds.overlap(iconbounds)) {
                    // entity is outside current view, move alert marker to edge of the screen
                    v2f centre(view.x + view.w / 2, view.y + view.h / 2);
                    v2f delta = (p - centre);
                    v2f halfsize = v2f(bounds.w, bounds.h) / 2;
                    float slope = delta.y / delta.x;

                    float testx = CLAMP(delta.x, -halfsize.x, halfsize.x);
                    float testy = testx * slope;
                    if (ABS(testy) > halfsize.y) {
                        testy = CLAMP(delta.y, -halfsize.y, halfsize.y);
                        testx = testy / slope;
                    }

                    p = centre + v2f(testx - 1, testy - 2);
                }

                render_push_sprite_w(&render, R_ID_ALERT, p, Z_ENT_PLAYER, false, 1);
            }
        }
    }
};
