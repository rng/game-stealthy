struct tri_t {
    v2f a, b, c;
};

struct line_t {
    v2f a, b;

    line_t(float x0, float y0, float x1, float y1) {
        a.x = x0;
        a.y = y0;
        b.x = x1;
        b.y = y1;
    }
};

static bool tri_cross2(tri_t points, tri_t triangle) {
    v2f pa = points.a;
    v2f pb = points.b;
    v2f pc = points.c;
    v2f p0 = triangle.a;
    v2f p1 = triangle.b;
    v2f p2 = triangle.c;
    float dXa = pa.x - p2.x;
    float dYa = pa.y - p2.y;
    float dXb = pb.x - p2.x;
    float dYb = pb.y - p2.y;
    float dXc = pc.x - p2.x;
    float dYc = pc.y - p2.y;
    float dX21 = p2.x - p1.x;
    float dY12 = p1.y - p2.y;
    float D = dY12 * (p0.x - p2.x) + dX21 * (p0.y - p2.y);
    float sa = dY12 * dXa + dX21 * dYa;
    float sb = dY12 * dXb + dX21 * dYb;
    float sc = dY12 * dXc + dX21 * dYc;
    float ta = (p2.y - p0.y) * dXa + (p0.x - p2.x) * dYa;
    float tb = (p2.y - p0.y) * dXb + (p0.x - p2.x) * dYb;
    float tc = (p2.y - p0.y) * dXc + (p0.x - p2.x) * dYc;
    if (D < 0) {
        return ((sa >= 0 && sb >= 0 && sc >= 0) || (ta >= 0 && tb >= 0 && tc >= 0) ||
                (sa + ta <= D && sb + tb <= D && sc + tc <= D));
    } else {
        return ((sa <= 0 && sb <= 0 && sc <= 0) || (ta <= 0 && tb <= 0 && tc <= 0) ||
                (sa + ta >= D && sb + tb >= D && sc + tc >= D));
    }
}

static bool tri_tri_intersect(tri_t t0, tri_t t1) {
    return !(tri_cross2(t0, t1) || tri_cross2(t1, t0));
}

static bool tri_rect_overlap(rect_t r, tri_t t) {
    tri_t rt0, rt1;

    rt0.a = v2f(r.x, r.y);
    rt0.b = v2f(r.x + r.w, r.y);
    rt0.c = v2f(r.x + r.w, r.y + r.h);

    rt1.a = v2f(r.x, r.y);
    rt1.b = v2f(r.x + r.w, r.y + r.h);
    rt1.c = v2f(r.x, r.y + r.h);

    return tri_tri_intersect(t, rt0) || tri_tri_intersect(t, rt1);
}

typedef struct {
    v2f center;  // position in game space (pixels)
    float angle; // facing angle (radians)
    float width; // width (angular spread) away from from facing angle of the cone (rads)
    int length;  // distance from center point the cone can see (pixels)
} vision_cone_t;

static bool point_is_visible(game_state_t *ctx, v2f pos) {
    // is the player in a light area
    if (light_probe(ctx->lighting, pos) > 0.1f) {
        // is the player not behind a box
        int fg_tile = tilemap_tile(&ctx->map, 4, pos.x / 8, pos.y / 8);
        if (fg_tile != 70) { // FG box tile
            return true;
        }
    }
    return false;
}

static entity_t *vision_cone_get_visible_entity(vision_cone_t *cone, game_state_t *ctx) {
    tri_t tri;
    float vs = (cone->width - RADS(2)) / 2;
    tri.a = cone->center;
    tri.b = tri.a + v2f(cos(cone->angle - vs) * cone->length, sin(cone->angle - vs) * cone->length);
    tri.c = tri.a + v2f(cos(cone->angle + vs) * cone->length, sin(cone->angle + vs) * cone->length);

    float minx = MIN(MIN(tri.a.x, tri.b.x), tri.c.x);
    float maxx = MAX(MAX(tri.a.x, tri.b.x), tri.c.x);
    float miny = MIN(MIN(tri.a.y, tri.b.y), tri.c.y);
    float maxy = MAX(MAX(tri.a.y, tri.b.y), tri.c.y);

    rect_t cone_bounds = rect_t(minx, miny, maxx - minx, maxy - miny);
    FOREACH_ENTITY_IN_BOUNDS(e, &ctx->entitymgr, cone_bounds, ENTITY_FLAG_PLAYER) {
        v2f epos = e->pos + v2f(4, 4);
        v2f delta = (epos - cone->center);
        rect_t cast_bounds(0, 0, 1, 1);
        v2f cast_pos = cone->center;
        auto r = collide_tilemap_check(&ctx->collide, &cast_pos, delta, cast_bounds);
        // can we actually see the player
        if (!(r.collx || r.colly) && tri_rect_overlap(entity_bounds(e), tri)) {
            if (point_is_visible(ctx, epos)) {
                return e;
            }
        }
    }
    return NULL;
}

typedef enum {
    VISCONE_IDLE = 0,
    VISCONE_ALERT,
    VISCONE_ALARM,
} vision_cone_state_t;

enum {
    TINT_GREEN = 16,
    TINT_RED = 32,
    TINT_YELLOW = 48,
};

static void vision_cone_draw_light(vision_cone_t &cone, lighting_t &lighting,
                                   vision_cone_state_t state, float ts) {
    switch (state) {
        case VISCONE_ALARM:
            light_push_spot(lighting, cone.center, cone.length, cone.width, cone.angle, TINT_RED);
            break;
        case VISCONE_ALERT:
            light_push_spot(lighting, cone.center, cone.length * 0.95f, cone.width * 0.8f,
                            cone.angle, TINT_YELLOW);
            light_push_spot(lighting, cone.center, cone.length * ts, cone.width, cone.angle,
                            TINT_RED);
            break;
        case VISCONE_IDLE:
            light_push_spot(lighting, cone.center, cone.length, cone.width, cone.angle, TINT_GREEN);
            break;
    }
};
