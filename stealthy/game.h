#pragma once

#include "engine/camera.h"
#include "engine/collide.h"
#include "engine/entitymgr.h"
#include "engine/render.h"
#include "engine/sound.h"

#include "asset_table.h"

#define GRAVITY (850.0f)
#define GAME_RES_WIDTH (64)
#define GAME_RES_HEIGHT (64)

enum {
    // render layers (ordered from furthest to nearest)
    Z_MAP_PARALLAX,
    Z_MAP_BG1,
    Z_MAP_BG2,
    Z_MAP_BASE,
    Z_ENT_BACKROUND,
    Z_ENT_BG3,
    Z_MAP_ROOM_1,
    Z_ENT_ENEMY,
    Z_ENT_PLAYER,
    Z_ENT_PARTICLE,
    Z_ENT_UI,
    Z_MAP_FG,
    // tile types
    COLLIDE_TILE_LADDER = 3,
    // entity flags
    ENTITY_FLAG_PLAYER = FLAG(1),
    ENTITY_FLAG_INTERACTIVE = FLAG(2),
    ENTITY_FLAG_ALERTABLE = FLAG(3),

    // events
    EVENT_INTERACT = 1,
    EVENT_TRIGGER,
    EVENT_ALERT,
    EVENT_DAMAGE,
    EVENT_EXIT,
    EVENT_PLAYER_ENTERED,
};

extern const entity_type_t player_type;
