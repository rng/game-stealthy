#include <stdlib.h>
#include "enemy_ai.h"
#include "engine/timeout.h"

// ----------------------------------------------------------------------------
//
//
//                                   LASER
//
//
// ----------------------------------------------------------------------------

struct laser_t : entity_t {
    float t;
    bool on;
    int alertrange;
    timeout_t spark;
};

void laser_tick(laser_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    self->t += input->dt;
    float space = 1 - self->t;
    if (self->t > 0.9f) {
        self->t = 0.1f;
    }

    bool player_hit = false;

    if (self->bounds.w == 8) {
        // vertical
        int sx = self->pos.x + 3;
        int sy = self->pos.y;
        int ey = sy + self->bounds.h;
        int my = sy + self->bounds.h * CLAMP(space, 0.1f, 0.9f);

        rect_t collide_bounds(0, 0, 1, 1);
        v2f collide_pos = self->pos + v2f(4, self->bounds.h - 3);
        v2f collide_vel(0, -(self->bounds.h - 4));
        auto r = collide_tilemap_check(&ctx->collide, &collide_pos, collide_vel, collide_bounds);
        if (r.colly) {
            sy = r.ry;
        }

        // alert nearby enemies if laser touched
        rect_t laser_bounds = rect_t(sx, sy, 1, ey - sy);
        FOREACH_ENTITY_IN_BOUNDS(player, em, laser_bounds, ENTITY_FLAG_PLAYER) {
            player_hit = true;
            int r = self->alertrange;
            rect_t bounds = rect_t(self->pos.x - r, self->pos.y - r, r * 2, r * 2);
            FOREACH_ENTITY_IN_BOUNDS(e, em, bounds, ENTITY_FLAG_ALERTABLE) {
                entity_send_event(e, EVENT_ALERT, player, ctx);
            }
        }

        // emit sparks
        int col = player_hit ? 9 : 11;
        if (timeout_expired(&self->spark, input->dt, 0)) {
            v2f vel = v2f(8 * RANDFLOAT(-5.0f, 5.0f), RANDFLOAT(0.2f, 2.0f));
            spark_t spark(v2f(sx, sy), vel, 0.1f, col);
            ctx->particles->sparks.spawn(spark);
            timeout_init(&self->spark, RANDFLOAT(0.1f, 0.2f));
        }

        // draw laser line with moving bit
        if (my <= sy) {
            render_push_line_w(&ctx->render, sx, sy, sx, ey, Z_ENT_PLAYER, col);
        } else {
            render_push_line_w(&ctx->render, sx, sy, sx, my - 1, Z_ENT_PLAYER, col);
            render_push_line_w(&ctx->render, sx, my, sx, my + 2, Z_ENT_PLAYER, col);
            render_push_line_w(&ctx->render, sx, my + 3, sx, my + 4, Z_ENT_PLAYER, col);
            render_push_line_w(&ctx->render, sx, my + 5, sx, ey, Z_ENT_PLAYER, col);
        }

        // draw glow
        light_push_point(ctx->lighting, self->pos + v2f(3.5, self->bounds.h + 3), 4);
        light_push_capsule(ctx->lighting, v2f(sx + 1, sy + 1), v2f(sx + 1, ey - 1), 1.8,
                           player_hit ? TINT_RED : TINT_GREEN);
    } else {
        // horizontal
        int sx = self->pos.x;
        int sy = self->pos.y + 3;
        int ex = sx + self->bounds.w;
        int mx = sx + self->bounds.w * CLAMP(space, 0.1f, 0.9f);

        rect_t collide_bounds(0, 0, 1, 1);
        v2f collide_pos = self->pos + v2f(self->bounds.w - 3, 4);
        v2f collide_vel(-(self->bounds.w - 4), 0);
        auto r = collide_tilemap_check(&ctx->collide, &collide_pos, collide_vel, collide_bounds);
        if (r.collx) {
            sx = r.rx;
        }

        // alert nearby enemies if laser touched
        rect_t laser_bounds = rect_t(sx, sy, ex - sx, 1);
        FOREACH_ENTITY_IN_BOUNDS(player, em, laser_bounds, ENTITY_FLAG_PLAYER) {
            player_hit = true;
            int r = self->alertrange;
            rect_t bounds = rect_t(self->pos.x - r, self->pos.y - r, r * 2, r * 2);
            FOREACH_ENTITY_IN_BOUNDS(e, em, bounds, ENTITY_FLAG_ALERTABLE) {
                entity_send_event(e, EVENT_ALERT, player, ctx);
            }
        }

        // emit sparks
        int col = player_hit ? 9 : 11;
        if (timeout_expired(&self->spark, input->dt, 0)) {
            v2f vel = v2f(8 * RANDFLOAT(0.5f, 3.0f), 8 * RANDFLOAT(-5.0f, 0.0f));
            spark_t spark(v2f(sx, sy), vel, 0.2f, col);
            ctx->particles->sparks.spawn(spark);
            timeout_init(&self->spark, RANDFLOAT(0.1f, 0.2f));
        }

        // draw laser line with moving bit
        if (mx <= sx) {
            render_push_line_w(&ctx->render, sx, sy, ex, sy, Z_ENT_PLAYER, col);
        } else {
            render_push_line_w(&ctx->render, sx, sy, mx - 1, sy, Z_ENT_PLAYER, col);
            render_push_line_w(&ctx->render, mx, sy, mx + 2, sy, Z_ENT_PLAYER, col);
            render_push_line_w(&ctx->render, mx + 3, sy, mx + 4, sy, Z_ENT_PLAYER, col);
            render_push_line_w(&ctx->render, mx + 5, sy, ex, sy, Z_ENT_PLAYER, col);
        }

        // draw glow
        light_push_point(ctx->lighting, self->pos + v2f(self->bounds.w + 3, 3.5), 4);
        light_push_capsule(ctx->lighting, v2f(sx + 1, sy + 1), v2f(ex - 1, sy + 1), 1.8f,
                           player_hit ? TINT_RED : TINT_GREEN);
    }
}

static void laser_event(laser_t *self, int event, void *param, void *ctx) {
    if (event == EVENT_TRIGGER) {
        self->on = !self->on;
    }
}

DEF_ENTITY_TYPE(laser, laser_new, laser_tick, laser_event, ENTITY_FLAG_COLLIDE);

void laser_new(entitymgr_t *em, v2f pos, v2i size, struct entity_props_t *props, int id) {
    rect_t bounds = {0, 0, size.x, size.y};
    auto self = entitymgr_new<laser_t>(em, pos, bounds, &laser_type, id);
    self->alertrange = props->get("alertrange", 50);
    self->on = true;
}

// ----------------------------------------------------------------------------
//
//
//                                   SCANNER
//
//
// ----------------------------------------------------------------------------

struct scanner_t : entity_t {
    enemy_ai_t ai;
    bool disabled;
    int dir;
};

static tri_t scanner_get_vision_cone(scanner_t *self) {
    return self->ai.get_vision_tri(entity_center(self));
}

static void scanner_do_idle(scanner_t *self, float dt, game_state_t *game) {
    self->ai.facing_a += self->dir * dt * 0.3f;
    if (self->ai.facing_a > self->ai.config.max_a) {
        self->ai.facing_a = self->ai.config.max_a;
        self->dir = -self->dir;
    } else if (self->ai.facing_a < self->ai.config.min_a) {
        self->ai.facing_a = self->ai.config.min_a;
        self->dir = -self->dir;
    }
}

static void scanner_tick(scanner_t *self, entitymgr_t *em, input_state_t *input,
                         game_state_t *ctx) {
    if (!self->disabled) {
        self->ai.tick(entity_center(self), ctx, input->dt, (entity_t *)self);
    }

    int frame = 0;
    for (int i = 0; i < 5; i++) {
        if (self->ai.facing_a <= RADS(22.5 + i * 45)) {
            frame = i;
            break;
        }
    }
    render_push_sprite_w(&ctx->render, R_ID_SCANNER, self->pos, Z_ENT_ENEMY, false, frame);
    light_push_point(ctx->lighting, self->pos + v2f(4, 4), 4);
}

static void scanner_event(scanner_t *self, int event, void *param, void *ctx) {
    if (event == EVENT_ALERT) {
        entity_t *player = (entity_t *)param;
        v2f pos = entity_center(self);
        v2f epos = entity_center(player);
        self->ai.force_face_target(pos, epos);
    } else if (event == EVENT_TRIGGER) {
        self->disabled = true;
    }
}

DEF_ENTITY_TYPE(scanner, scanner_new, scanner_tick, scanner_event,
                ENTITY_FLAG_COLLIDE | ENTITY_FLAG_ALERTABLE);

void scanner_new(entitymgr_t *em, v2f pos, v2i size, struct entity_props_t *props, int id) {
    rect_t bounds = {0, 0, 8, 8};
    auto self = entitymgr_new<scanner_t>(em, pos, bounds, &scanner_type, id);
    self->ai.config = (ai_config_t){
        .vision_width = RADS(30),
        .vision_length = 70,
        .vision_a_limits = true,
        .min_a = RADS(props->get("mina", 45)),
        .max_a = RADS(props->get("maxa", 135)),
        .alert_rate = 1.5f,
        .dealert_rate = 2.f,
        .track_speed = 5.f,
        .alert_range = (int)props->get("alertrange", 50),
        .idle_behaviour = (ai_callback_t)scanner_do_idle,
    };
    self->ai.facing_a = self->ai.config.min_a;
    self->dir = 1;
}

// ----------------------------------------------------------------------------
//
//
//                                 ENEMYSHOT
//
//
// ----------------------------------------------------------------------------

struct enemyshot_t : entity_t {
    anim_t anim;
    v2f norm;
    bool flip;
    bool hit;
};

static const uint16_t _enemyshot_frames[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
static const anim_seq_t enemyshot_anim_seqs[] = {
    {_enemyshot_frames, NELEMS(_enemyshot_frames), false},
};

static void enemyshot_tick(enemyshot_t *self, entitymgr_t *em, input_state_t *input,
                           game_state_t *ctx) {
    auto bounds = entity_bounds(self);
    FOREACH_ENTITY_IN_BOUNDS(player, em, bounds, ENTITY_FLAG_PLAYER) {
        if (!self->hit) {
            entity_send_event(player, EVENT_DAMAGE, self, ctx);
            self->hit = true;
        }
        break;
    }

    if (anim_update(&self->anim, input->dt)) {
        entitymgr_remove(em, self);
    }

    render_push_sprite_w(&ctx->render, R_ID_LEGENEMYSHOT, self->pos, Z_ENT_ENEMY, self->flip,
                         self->anim.frame);
    light_push_point(ctx->lighting, self->pos, 2);
}

DEF_ENTITY_TYPE(enemyshot, enemyshot_new, enemyshot_tick, NULL, ENTITY_FLAG_COLLIDE);

enemyshot_t *enemyshot_new_ex(entitymgr_t *em, v2f pos, v2f norm) {
    rect_t bounds = {0, 8, 43, 4};
    if (norm.x < 0) {
        pos = pos - v2f(43, 0);
    }
    auto self = entitymgr_new<enemyshot_t>(em, pos - v2f(0, 7), bounds, &enemyshot_type, -1);
    anim_init_sequences(&self->anim, .1, enemyshot_anim_seqs);
    anim_play(&self->anim, 0);
    self->norm = norm;
    self->flip = norm.x < 0;
    self->hit = false;
    return self;
}

void enemyshot_new(entitymgr_t *em, v2f pos, v2i size, struct entity_props_t *props, int id) {
    enemyshot_new_ex(em, pos, v2f(0, 0));
}

// ----------------------------------------------------------------------------
//
//
//                                   ENEMY
//
//
// ----------------------------------------------------------------------------

typedef enum enemy_state_t { ENEMY_IDLE, ENEMY_WALK, ENEMY_ATTACK } enemy_state_t;

#define ENEMY_MOVEVEL (20)
#define ENEMY_ATTACK_TIME_SEC (1)
#define ENEMY_STAND_TIME_SEC (5)

struct enemy_t : entity_t {
    enemy_ai_t ai;
    v2f vel;
    anim_t anim;
    int cur_waypoint_id;
    timeout_t attack_time;
    timeout_t stand_time;
    enemy_state_t state;
    int frames; // Use different frames for different animations.
    bool fired;
};

static const uint16_t _enemy_frames_walk[] = {0, 1, 2, 3};
static const uint16_t _enemy_frames_stand[] = {0, 1, 2, 3};
static const uint16_t _enemy_frames_shoot[] = {0, 1, 2, 2, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
static const anim_seq_t enemy_anim_seqs[] = {
    {_enemy_frames_walk, NELEMS(_enemy_frames_walk), true},
    {_enemy_frames_stand, NELEMS(_enemy_frames_stand), true},
    {_enemy_frames_shoot, NELEMS(_enemy_frames_shoot), false},
};

tri_t enemy_get_vision_cone(enemy_t *self) {
    v2f cone_pos = entity_center(self) + ((self->vel.x > 0) ? v2f(6, -6) : v2f(-6, -6));
    return self->ai.get_vision_tri(cone_pos);
}

static void enemy_do_idle(enemy_t *self, float dt, game_state_t *game) {
    // get the current waypoint we're moving toward
    auto cur_waypoint = (waypoint_t *)entitymgr_find_by_id(&game->entitymgr, self->cur_waypoint_id);
    v2f dist_to_waypoint = entity_center(cur_waypoint) - entity_center(self);

    // printf("dist_to_waypoint: %f\n", (double)dist_to_waypoint.len());

    if ((dist_to_waypoint.len() < 8) && (self->state == ENEMY_WALK)) {
        // we've reached the waypoint, wait for a bit and then get next waypoint
        self->state = ENEMY_IDLE;
        self->frames = R_ID_LEGENEMYSTAND;
        anim_play(&self->anim, 1);
        self->vel.x = 0;
        timeout_init(&self->stand_time, ENEMY_STAND_TIME_SEC);
    }

    if (self->state == ENEMY_IDLE) {
        // standing
        if (timeout_expired(&self->stand_time, dt, 0)) {
            // Time to start walking again
            self->state = ENEMY_WALK;
            self->frames = R_ID_LEGENEMYWALK;
            anim_play(&self->anim, 0);
            self->cur_waypoint_id = cur_waypoint->next_id;
        }
    } else {
        // Walking. Move toward current waypoint
        if (dist_to_waypoint.x > 0) {
            self->vel.x = ENEMY_MOVEVEL;
        } else {
            self->vel.x = -ENEMY_MOVEVEL;
        }
    }

    self->ai.facing_a = (self->vel.x > 0) ? RADS(10) : RADS(170);
}

static void enemy_do_alert(enemy_t *self, float dt, game_state_t *game) {
    anim_play(&self->anim, 0);
    entity_t *player = entitymgr_find_by_flag(&game->entitymgr, ENTITY_FLAG_PLAYER, NULL);
    v2f dist_to_player = entity_center(player) - entity_center(self);
    if (dist_to_player.x > 0) {
        self->vel.x = ENEMY_MOVEVEL;
    } else {
        self->vel.x = -ENEMY_MOVEVEL;
    }
    self->frames = R_ID_LEGENEMYWALK;
}

static void enemy_do_chase(enemy_t *self, float dt, game_state_t *game) {
    bool can_attack = timeout_expired(&self->attack_time, dt, 0);
    self->fired = false;

    entity_t *player = entitymgr_find_by_flag(&game->entitymgr, ENTITY_FLAG_PLAYER, NULL);
    v2f dist_to_player = entity_center(player) - entity_center(self);

    if ((abs(dist_to_player.x) < 40) && can_attack) {
        // Start attack sequence
        timeout_init(&self->attack_time, ENEMY_ATTACK_TIME_SEC);
        self->state = ENEMY_ATTACK;
        self->frames = R_ID_LEGENEMYSHOOT;
        anim_play(&self->anim, 2);
        self->vel.x = 0;
    } else {
        // otherwise, move toward the player
        if (dist_to_player.x > 0) {
            self->vel.x = ENEMY_MOVEVEL;
        } else {
            self->vel.x = -ENEMY_MOVEVEL;
        }
        self->state = ENEMY_WALK;
        self->frames = R_ID_LEGENEMYWALK;
        self->anim.frametime = .2f;
        anim_play(&self->anim, 0);
    }
}

static void enemy_do_attack(enemy_t *self, float dt, game_state_t *game) {
    self->vel.x = 0;
    entity_t *player = entitymgr_find_by_flag(&game->entitymgr, ENTITY_FLAG_PLAYER, NULL);
    if (player) {
        v2f dist_to_player = entity_center(player) - entity_center(self);
        if (!self->fired) {
            v2f shotdir(dist_to_player.norm().x, 0);
            enemyshot_new_ex(&game->entitymgr, entity_center(self), shotdir);
            self->fired = true;
        }
        self->frames = R_ID_LEGENEMYSHOOT;
        anim_play(&self->anim, 2);
    }
}

void enemy_tick(enemy_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    bool flip = fabs(self->ai.facing_a) > (float)M_PI / 2.f;
    v2f cone_pos = entity_center(self) + (flip ? v2f(-6, 0) : v2f(6, 0));

    self->ai.tick(cone_pos, ctx, input->dt, (entity_t *)self);

    anim_update(&self->anim, input->dt);

    v2f v = self->vel * input->dt;
    collide_res_t r = collide_tilemap_check(&ctx->collide, &self->pos, v, self->bounds);
    if (r.collx) {
        auto cur_waypoint = (waypoint_t *)entitymgr_find_by_id(em, self->cur_waypoint_id);
        self->cur_waypoint_id = cur_waypoint->next_id;
        self->vel.x = 0;
    }

    light_push_point(ctx->lighting, self->pos + v2f(flip ? 12 : 4, 8), 12);
    render_push_sprite_w(&ctx->render, self->frames, self->pos + v2f(0, 1), Z_ENT_ENEMY, flip,
                         self->anim.frame);
}

void enemy_event(enemy_t *self, int event, void *param, void *ctx) {
    if (event == EVENT_ALERT) {
        game_state_t *game = (game_state_t *)ctx;
        entity_t *player = (entity_t *)param;
        auto alert_pos = entity_center(player);
        self->ai.alert_from_other(game, self, alert_pos);
    }
}

DEF_ENTITY_TYPE(enemy, enemy_new, enemy_tick, enemy_event,
                ENTITY_FLAG_COLLIDE | ENTITY_FLAG_ALERTABLE);

void enemy_new(entitymgr_t *em, v2f pos, v2i size, struct entity_props_t *props, int id) {
    rect_t bounds = {0, 3, 13, 15};
    auto self = entitymgr_new<enemy_t>(em, pos, bounds, &enemy_type, id);
    self->ai.config = (ai_config_t){
        .vision_width = RADS(40),
        .vision_length = 40,
        .vision_a_limits = false,
        .alert_rate = 1.5f,
        .dealert_rate = 2.f,
        .track_speed = 5.f,
        .attack_range = 40,
        .reload_time = 5,
        .alert_range = (int)props->get("alertrange", 50),
        .idle_behaviour = (ai_callback_t)enemy_do_idle,
        .alert_behaviour = (ai_callback_t)enemy_do_alert,
        .chase_behaviour = (ai_callback_t)enemy_do_chase,
        .attack_behaviour = (ai_callback_t)enemy_do_attack,
    };
    self->vel = (v2f){0, 0};
    anim_init_sequences(&self->anim, .2, enemy_anim_seqs);
    self->cur_waypoint_id = props->get("patrol", -1);
    self->state = ENEMY_WALK;
    self->frames = R_ID_LEGENEMYWALK;
}

// ----------------------------------------------------------------------------
//
//
//                                    SHOT
//
//
// ----------------------------------------------------------------------------

struct shot_t : entity_t {
    v2f vel;
};

static void shot_tick(shot_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    v2f v = self->vel * input->dt;
    collide_res_t r = collide_tilemap_check(&ctx->collide, &self->pos, v, self->bounds);
    if (r.collx || r.colly) {
        entitymgr_remove(em, self);
    } else {
        auto bounds = entity_bounds(self);
        FOREACH_ENTITY_IN_BOUNDS(player, em, bounds, ENTITY_FLAG_PLAYER) {
            entity_send_event(player, EVENT_DAMAGE, self, ctx);
            entitymgr_remove(em, self);
            break;
        }
    }

    int frame = (ABS(v.x) > ABS(v.y)) ? 0 : 2;
    render_push_sprite_w(&ctx->render, R_ID_SHOT, self->pos - v2f(1, 1), Z_ENT_ENEMY, false, frame);
    light_push_point(ctx->lighting, self->pos, 2);
}

DEF_ENTITY_TYPE(shot, shot_new, shot_tick, NULL, ENTITY_FLAG_COLLIDE);

shot_t *shot_new_ex(entitymgr_t *em, v2f pos, v2f norm, float speed) {
    rect_t bounds = {-1, -1, 3, 3};
    auto self = entitymgr_new<shot_t>(em, pos, bounds, &shot_type, -1);
    self->vel = norm * speed;
    return self;
}

void shot_new(entitymgr_t *em, v2f pos, v2i size, struct entity_props_t *props, int id) {
    shot_new_ex(em, pos, v2f(0, 0), 0);
}

// ----------------------------------------------------------------------------
//
//
//                                  SHOOTER
//
//
// ----------------------------------------------------------------------------

#define SHOOTER_ATTACK_TIME (1)

struct shooter_t : entity_t {
    int angle;
    int shotspeed;
    anim_t anim;
    timeout_t attack_time;
};

static const uint16_t _shooter_frames_idle[] = {0, 1};
static const anim_seq_t shooter_anim_seqs[] = {
    {_shooter_frames_idle, NELEMS(_shooter_frames_idle), true},
};

static void shooter_fire(shooter_t *self, game_state_t *game) {
    if (timeout_expired(&self->attack_time, 0, SHOOTER_ATTACK_TIME)) {
        v2f dir(cosf(RADS(self->angle)), sinf(RADS(self->angle)));
        shot_new_ex(&game->entitymgr, self->pos + v2f(3, 3) + (dir * 2), dir, self->shotspeed);
    }
}

static void shooter_tick(shooter_t *self, entitymgr_t *em, input_state_t *input,
                         game_state_t *ctx) {
    timeout_expired(&self->attack_time, input->dt, 0);
    anim_update(&self->anim, input->dt);

    int dirofs;
    if (self->angle == 0) {
        dirofs = 4;
    } else if (self->angle == 90) {
        dirofs = 2;
    } else if (self->angle == 180) {
        dirofs = 0;
    } else {
        ASSERT(!"unsupported shooter angle");
    }
    render_push_sprite_w(&ctx->render, R_ID_SHOOTER, self->pos, Z_ENT_ENEMY, false,
                         self->anim.frame + dirofs);
    light_push_point(ctx->lighting, self->pos + v2f(4, 4), 4);
}

static void shooter_event(shooter_t *self, int event, void *param, void *ctx) {
    if (event == EVENT_ALERT || event == EVENT_TRIGGER) {
        shooter_fire(self, (game_state_t *)ctx);
    }
}

DEF_ENTITY_TYPE(shooter, shooter_new, shooter_tick, shooter_event,
                ENTITY_FLAG_COLLIDE | ENTITY_FLAG_ALERTABLE);

void shooter_new(entitymgr_t *em, v2f pos, v2i size, struct entity_props_t *props, int id) {
    rect_t bounds = {0, 0, size.x, size.y};
    auto self = entitymgr_new<shooter_t>(em, pos, bounds, &shooter_type, id);
    anim_init_sequences(&self->anim, .3, shooter_anim_seqs);
    self->angle = props->get("angle");
    self->shotspeed = props->get("speed", 200);
}
