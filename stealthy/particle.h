#include <new>

struct particle_t {
    uint16_t x, y;
    bool alive;
};

template <typename T, const int N> struct emitter_t {
    T particles[N];

    bool spawn(int x, int y) {
        for (int i = 0; i < N; i++) {
            if (!particles[i].alive) {
                void *addr = &particles[i];
                T *p = new (addr) T(x, y);
                p->alive = true;
                return true;
            }
        }
        return false;
    }

    bool spawn(T t) {
        for (int i = 0; i < N; i++) {
            if (!particles[i].alive) {
                particles[i] = t;
                particles[i].alive = true;
                return true;
            }
        }
        return false;
    }

    void tick(float dt, void *ctx) {
        for (int i = 0; i < N; i++) {
            if (particles[i].alive) {
                particles[i].tick(dt, ctx);
            }
        }
    }
};
