struct spark_t : particle_t {
    float t;
    int c;
    v2f v, p;

    spark_t(v2f pos, v2f vel, float l, int col) {
        p = pos;
        v = vel;
        t = l;
        c = col;
    }

    void tick(float dt, void *ctx) {
        game_state_t *gs = (game_state_t *)ctx;

        rect_t bounds(0, 0, 2, 2);
        collide_res_t r = collide_tilemap_check(&gs->collide, &p, v * dt, bounds);
        if (r.colly) {
            v.y *= -0.5f;
        }
        v.y += GRAVITY * dt;

        v2f p1 = p + v.norm() * 2;
        render_push_line_w(&gs->render, p.x, p.y, p1.x, p1.y, Z_ENT_BACKROUND, c);
        light_push_point(gs->lighting, p, 2);

        t -= dt;
        if (t <= 0) {
            alive = false;
        }
    }
};
