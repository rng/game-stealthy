#pragma once

#include <stdint.h>

struct interaction_t {
    int asset, anim_frames;
};

struct interactive_entity_t : entity_t {
    bool disabled, hover_hidden;
};

// ----------------------------------------------------------------------------
//
//
//                                   PLAYER
//
//
// ----------------------------------------------------------------------------

struct player_t {
    entity_t e;
    v2f vel, kickback;
    bool flip;
    bool on_ground, on_ladder, on_exit;
    bool prev_on_ground, prev_on_ladder;
    anim_t anim;
    int health;
    // Previous time when the player walked off a ground tile.
    float falling_timestamp;
    // Jump buffer of a depth of 1. Stores the time of the last jump button press.
    float jump_timestamp;
    // The entity player is currently hovered over. Can only hover one interactive entity at a time.
    entity_t *hovered_ent, *exit_ent;
    // look support
    v2f camofs;
    // invulnerability after hit
    timeout_t invuln_time, invuln_flicker;
    bool flicker;
};

#define PLAYER_CLIMBVEL 40
#define PLAYER_MOVEVEL 80
#define PLAYER_JUMPVEL 175
#define PLAYER_LOOKVEL 100
#define PLAYER_UNLOOKVEL 200
#define PLAYER_LOOKUP_DIST 20
#define PLAYER_LOOKDOWN_DIST 30
#define COYOTE_TIME_DT 0.1f // Unit is seconds
#define JUMP_BUFFER_DT 0.1f

static const uint16_t _frames_idle[] = {0, 0, 1, 1, 2, 2, 3, 3};
static const uint16_t _frames_walk[] = {4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9};
static const uint16_t _frames_jump_hold[] = {10};
static const uint16_t _frames_jump_release[] = {11, 12, 13, 14};
static const uint16_t _frames_climb[] = {15, 15, 16, 16, 17, 17, 18, 18};
static const uint16_t _frames_on_ladder[] = {16};
static const anim_seq_t anim_seqs[] = {
    {_frames_idle, NELEMS(_frames_idle), true},
    {_frames_walk, NELEMS(_frames_walk), true},
    {_frames_jump_hold, NELEMS(_frames_jump_hold), false},
    {_frames_jump_release, NELEMS(_frames_jump_release), false},
    {_frames_climb, NELEMS(_frames_climb), true},
    {_frames_on_ladder, NELEMS(_frames_on_ladder), false},
};

enum {
    IDLE = 0,
    WALK,
    JUMP_HOLD,
    JUMP_RELEASE,
    CLIMB,
    ON_LADDER,
};

static void update_on_ground(player_t *self, bool on_ground) {
    self->prev_on_ground = self->on_ground;
    self->on_ground = on_ground;
}

static void jump(player_t *self) {
    self->vel.y = -PLAYER_JUMPVEL;
    update_on_ground(self, false);
}

static bool check_slidejump(player_t *self, int dy, int buffer, game_state_t *ctx, float &newx) {
    /*
     Check if we can jump (up or down depending on `dy`), with a "slide buffer". Assuming we
     have a hole above the player exactly the same width as the player. The slide jump is
     allowed if the error in alignment between the player and the hole is less than `buffer`.
     Return true if the slide jump is allowed, with the new x position for the jump in `newx`.

           hole
     +-------------+
                  player
     < buffer >+------------+

     */
    float ts = ctx->collide.tilesize;
    float px = self->e.pos.x + self->e.bounds.x;
    float py = self->e.pos.y + self->e.bounds.y;
    int sx = floorf(px / ts);
    int ex = ceilf((px + self->e.bounds.w) / ts);
    int y = floorf(py / ts) + dy;
    for (int x = sx; x < ex; x++) {
        int t = ctx->collide.tile(ctx->collide.context, x, y);
        if (t != COLLIDE_TILE_IMPASSABLE) {
            float delta = (px - (x * ts));
            if (delta < buffer && delta >= -buffer) {
                newx = x * ts - self->e.bounds.x;
                return true;
            }
        }
    }
    return false;
}

static void check_collision(player_t *self, float dt, game_state_t *ctx) {
retry_slide:
    if (self->kickback.len() > 0) {
        self->vel += self->kickback;
        self->kickback = v2f(0, 0);
    }
    v2f v = self->vel * dt;
    v2f oldpos = self->e.pos;
    collide_res_t r = collide_tilemap_check(&ctx->collide, &self->e.pos, v, self->e.bounds);
    if (r.collx) {
        self->vel.x = 0;
    }
    if (r.colly) {
        // on vertical collision with a tile below the character, set our
        // `on_ground` state so we know if we can jump
        // This prevents ceiling collisions as counting as touching the ground.
        if (self->vel.y > 0) {
            update_on_ground(self, true);

            // Just landed
            if (self->on_ground && !self->prev_on_ground) {
                sound_play_note(&ctx->mixer, S_ID_LAND, SOUND_NOTE_C4, SND_VOLUME_SFX);
            }
        } else if (self->vel.y < 0) {
            // check if we are jumping up into a corner and can slide around the edge
            float newx;
            if (check_slidejump(self, -1, 4, ctx, newx)) {
                self->e.pos = v2f(newx, oldpos.y);
                self->vel.x = 0;
                goto retry_slide;
            }
        }
        self->vel.y = 0;
    } else if (self->vel.y > 0) { // If falling
        update_on_ground(self, false);
    }
}

static bool can_jump(player_t *self, game_state_t *ctx) {
    // Basic case
    if (self->on_ground || self->on_ladder) {
        return true;
    }

    // Coyote Time
    // If falling
    if (self->vel.y > 0) {
        if ((ctx->time - self->falling_timestamp) < COYOTE_TIME_DT) {
            return true;
        }
    }

    return false;
}

static void jump_if_input_buffered(player_t *self, game_state_t *ctx) {
    // If can jump, check if we should jump if the jump input was pressed
    // recently enough. This gives players a buffer to input jumps when
    // they want to chain consecutive jumps.

    if (!can_jump(self, ctx)) {
        return;
    }

    if ((ctx->time - self->jump_timestamp) < JUMP_BUFFER_DT) {
        jump(self);
    }
}

static void process_keys(player_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    rect_t bounds = rect_t(self->e.pos.x, self->e.pos.y, self->e.bounds.w, self->e.bounds.h);
    self->prev_on_ladder = self->on_ladder;
    int anim = IDLE;

    // latch onto ladder if we're touching a ladder tile and pressing up
    if (collide_tilemap_touching_tile(&ctx->collide, &bounds, COLLIDE_TILE_LADDER)) {
        if ((!self->on_ladder) && input->pressed(BUTTON_UP_BIT)) {
            self->on_ladder = true;
        }
        anim = ON_LADDER;
    } else {
        self->on_ladder = false;
    }

    // Show that the object is interactive by displaying a menu when the object is hovered.
    entity_t *new_hover = NULL;
    FOREACH_ENTITY_IN_BOUNDS(e, em, bounds, ENTITY_FLAG_INTERACTIVE) {
        // if entity is still interactive (not disabled), send B and C button state/changes to it
        if (!((interactive_entity_t *)e)->disabled) {
            new_hover = e;
            if (input->changed(BUTTON_B_BIT | BUTTON_C_BIT) ||
                input->pressed(BUTTON_B_BIT | BUTTON_C_BIT)) {
                entity_send_event(e, EVENT_INTERACT, (void *)input, ctx);
            }
            break;
        }
    }
    // if we're changing which entitiy is hovered, send the last entity an empty input to reset it
    if (self->hovered_ent && (self->hovered_ent != new_hover)) {
        input_state_t empty_input = {};
        entity_send_event(self->hovered_ent, EVENT_INTERACT, (void *)&empty_input, ctx);
    }
    self->hovered_ent = new_hover;

    if (self->on_exit) {
        // handle exit motion
        // float alpha = sinf(2 * self->exit_ent->t);
        auto exit_pos = self->exit_ent->pos + v2f(16, 24); // + 3 * alpha);
        v2f delta = exit_pos - (self->e.pos + v2f(4, 4));
        // v2f accel = delta * 20.f;
        // self->vel += accel * input->dt;
        self->vel = delta * 20.f;
        self->vel = self->vel.clamp(-20, 20, -20, 20);
    } else {
        // handle normal motion

        // jumping
        if (input->pressed(BUTTON_A_BIT)) {
            // detach from ladder if we jump
            self->on_ladder = false;
            if (input->clicked(BUTTON_A_BIT) && input->pressed(BUTTON_DOWN_BIT)) {
                // check for jump down through one-way platform
                rect_t jd = entity_bounds(&self->e);
                jd.h += 4;

                float newx;
                if (check_slidejump(self, 1, 6, ctx, newx)) {
                    self->e.pos = v2f(newx, self->e.pos.y + 1);
                }
            } else if (input->clicked(BUTTON_A_BIT)) {
                // normal upward jump
                sound_play_note(&ctx->mixer, S_ID_JUMP, SOUND_NOTE_C4, SND_VOLUME_SFX);

                self->jump_timestamp = ctx->time;
                if (can_jump(self, ctx)) {
                    jump(self);
                }
            }
            anim = JUMP_HOLD;
        } else if (self->vel.y < 0) { // Already mid-jump
            // reduce jump velocity if jump button is not held down
            // (allows for jump height control)
            self->vel.y /= 4.0f;
            anim = JUMP_RELEASE;
        } else if (self->vel.y > 0) {
            anim = JUMP_RELEASE;
        }
        jump_if_input_buffered(self, ctx);
        // climbing ladders
        if (self->on_ladder && !input->pressed(BUTTON_A_BIT)) {
            anim = CLIMB;
            if (input->pressed(BUTTON_UP_BIT)) {
                self->vel.y = -PLAYER_CLIMBVEL;
            } else if (input->pressed(BUTTON_DOWN_BIT)) {
                self->vel.y = PLAYER_MOVEVEL;
            } else {
                self->vel.y = 0;
                anim = ON_LADDER;
            }
        }
        // look up/down
        if (self->on_ground && !self->on_ladder && input->pressed(BUTTON_UP_BIT)) {
            if ((self->camofs.y > -PLAYER_LOOKUP_DIST)) {
                self->camofs.y -= PLAYER_LOOKVEL * input->dt;
            }
        } else if (self->on_ground && !self->on_ladder && input->pressed(BUTTON_DOWN_BIT)) {
            if ((self->camofs.y < PLAYER_LOOKDOWN_DIST)) {
                self->camofs.y += PLAYER_LOOKVEL * input->dt;
            }
        } else {
            float v = PLAYER_UNLOOKVEL * input->dt;
            self->camofs.y += CLAMP(-self->camofs.y, -v, v);
        }
        // gravity
        if (!self->on_ladder) {
            // only need to ignore gravity if on ladder, otherwise collision system handles it for
            // us
            self->vel.y += .75f * GRAVITY * input->dt;
        }
        // movement
        self->vel.x = 0;
        if (input->pressed(BUTTON_LEFT_BIT)) {
            self->flip = true;
            self->vel.x = -PLAYER_MOVEVEL;
            if (self->on_ground) {
                anim = WALK;
            }
        } else if (input->pressed(BUTTON_RIGHT_BIT)) {
            self->flip = false;
            self->vel.x = PLAYER_MOVEVEL;
            if (self->on_ground) {
                anim = WALK;
            }
        }
        if (!self->on_ground && !self->on_ladder) {
            // anim = JUMP;
        }
    }
    anim_play(&self->anim, anim);
}

static void player_update_falling_timestamp(player_t *self, game_state_t *ctx) {
    // if just started falling, then update timestamp
    if ((self->prev_on_ground || self->prev_on_ladder) && (!self->on_ground && !self->on_ladder) &&
        (self->vel.y > 0)) {
        self->falling_timestamp = ctx->time;
    }
}

static void player_render(player_t *self, input_state_t *input, game_state_t *ctx) {
    bool invuln = self->invuln_time.t > 0;
    timeout_expired(&self->invuln_time, input->dt, 0);
    if (timeout_expired(&self->invuln_flicker, input->dt, 0.08f)) {
        self->flicker = !self->flicker;
    }
    render_push_sprite_w(&ctx->render, R_ID_PLAYER, self->e.pos, Z_ENT_PLAYER, self->flip,
                         self->anim.frame + ((invuln && self->flicker) ? 10 : 0));
}

static void player_tick(player_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    process_keys(self, (entitymgr_t *)em, input, ctx);
    anim_update(&self->anim, input->dt);
    check_collision(self, input->dt, ctx);
    player_update_falling_timestamp(self, ctx);
    player_render(self, input, ctx);
}

static void player_event(player_t *self, int event, void *param, void *ctx) {
    game_state_t *gs = (game_state_t *)ctx;
    switch (event) {
        case EVENT_DAMAGE:
            if (self->invuln_time.t <= 0) {
                sound_play_note(&gs->mixer, S_ID_HIT, SOUND_NOTE_C4, SND_VOLUME_SFX);
#if (!INF_LIVES)
                self->health--;
#endif
                auto attacker = (entity_t *)param;
                self->kickback = (self->e.pos - attacker->pos).norm() * 5000;
                self->kickback.y = 0;
                timeout_init(&self->invuln_time, 1.5f);
                if (self->health == 0) {
                    scene_switch(&gs->scenes, SCENE_DIE, transition_fade, 2.5, true);
                }
            }
            break;
        case EVENT_EXIT:
            if (self->on_exit) {
                break;
            }
            sound_play_note(&gs->mixer, S_ID_WIN, SOUND_NOTE_C4, SND_VOLUME_SFX);
            self->on_exit = true;
            self->exit_ent = (entity_t *)param;
            scene_switch(&gs->scenes, SCENE_LEVELEND, transition_fade, 2.5, true);
            break;
    }
}

DEF_ENTITY_TYPE(player, player_new, player_tick, player_event,
                ENTITY_FLAG_COLLIDE | ENTITY_FLAG_PLAYER);

static void player_new(entitymgr_t *em, v2f pos, v2i size, entity_props_t *props, int id) {
    rect_t bounds = {1, 2, 8, 8};
    auto self = entitymgr_new<player_t>(em, pos, bounds, &player_type, id);
    self->flip = false;
    self->vel = (v2f){0, 0};
    self->health = 3;
    anim_init_sequences(&self->anim, 0.06, anim_seqs);
    self->prev_on_ground = true; // ALICE FIXME - do these default values work?
    self->prev_on_ladder = false;
    self->hovered_ent = NULL;
}

// ----------------------------------------------------------------------------
//
//
//                                   LIGHT
//
//
// ----------------------------------------------------------------------------

struct light_t : entity_t {
    bool on;
    float width, angle;
    int brightness;
    int trigger_id;
};

void light_tick(light_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    render_push_sprite_w(&ctx->render, R_ID_LIGHT, self->pos, Z_ENT_ENEMY, false, 0);
    if (self->on) {
        v2f pos = self->pos + v2f(3, 2);
        light_push_point(ctx->lighting, pos, 2);
        light_push_spot(ctx->lighting, pos, self->brightness, self->width, self->angle, 0);
    }
}

void light_event(light_t *self, int event, void *param, void *ctx) {
    game_state_t *gs = (game_state_t *)ctx;
    if (event == EVENT_TRIGGER) {
        if (self->on) {
            for (int i = 0; i < 4; i++) {
                v2f vel = v2f(10 * RANDFLOAT(-5.0f, 5.0f), RANDFLOAT(0.2f, 2.0f));
                spark_t spark(self->pos + v2f(3, 6), vel, 0.8f, 7);
                gs->particles->sparks.spawn(spark);
            }
        }
        self->on = !self->on;
        if (self->trigger_id != -1) {
            auto next_trigger = entitymgr_find_by_id(&gs->entitymgr, self->trigger_id);
            entity_send_event(next_trigger, EVENT_TRIGGER, param, ctx);
        }
    }
}

DEF_ENTITY_TYPE(light, light_new, light_tick, light_event, ENTITY_FLAG_COLLIDE);

void light_new(entitymgr_t *em, v2f pos, v2i size, struct entity_props_t *props, int id) {
    rect_t bounds = {0, 0, 8, 8};
    auto self = entitymgr_new<light_t>(em, pos, bounds, &light_type, id);
    self->on = props->get("on", true);
    self->trigger_id = props->get("trigger", -1);
    self->width = RADS(props->get("width", 60)); // how far to spread from the facing direction
    self->angle = RADS(props->get("angle", 90)); // facing angle
    self->brightness = props->get("bright", 70); // how bright / far the light emits.
}

// ----------------------------------------------------------------------------
//
//
//                                  WAYPOINT
//
//
// ----------------------------------------------------------------------------

struct waypoint_t : entity_t {
    int next_id;
};

DEF_ENTITY_TYPE(waypoint, waypoint_new, NULL, NULL, 0);

void waypoint_new(entitymgr_t *em, v2f pos, v2i size, struct entity_props_t *props, int id) {
    rect_t bounds = {0, 0, 2, 2};
    auto self = entitymgr_new<waypoint_t>(em, pos, bounds, &waypoint_type, id);
    self->next_id = props->get("next", -1);
}

// ----------------------------------------------------------------------------
//
//
//                                   TRIGGER
//
//
// ----------------------------------------------------------------------------

struct trigger_t : entity_t {
    bool triggered;
    int target_id;
};

void trigger_tick(trigger_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    if (!self->triggered) {
        rect_t bounds = entity_bounds(self);
        FOREACH_ENTITY_IN_BOUNDS(e, em, bounds, ENTITY_FLAG_PLAYER | ENTITY_FLAG_ALERTABLE) {
            auto target = entitymgr_find_by_id(em, self->target_id);
            entity_send_event(target, EVENT_TRIGGER, self, ctx);
            self->triggered = true;
            break;
        }
    }
}

DEF_ENTITY_TYPE(trigger, trigger_new, trigger_tick, NULL, 0);

void trigger_new(entitymgr_t *em, v2f pos, v2i size, struct entity_props_t *props, int id) {
    rect_t bounds = {0, 0, size.x, size.y};
    auto self = entitymgr_new<trigger_t>(em, pos, bounds, &trigger_type, id);
    self->triggered = false;
    self->target_id = props->get("trigger", -1);
}

// ----------------------------------------------------------------------------
//
//
//                                     TIMER
//
//
// ----------------------------------------------------------------------------

struct timetrigger_t : entity_t {
    float period, t;
    int target_id;
};

void timetrigger_tick(timetrigger_t *self, entitymgr_t *em, input_state_t *input,
                      game_state_t *ctx) {
    self->t += input->dt;
    if (self->t >= self->period) {
        self->t -= self->period;

        auto target = entitymgr_find_by_id(em, self->target_id);
        entity_send_event(target, EVENT_TRIGGER, self, ctx);
    }
}

DEF_ENTITY_TYPE(timetrigger, timetrigger_new, timetrigger_tick, NULL, 0);

void timetrigger_new(entitymgr_t *em, v2f pos, v2i size, struct entity_props_t *props, int id) {
    rect_t bounds = {0, 0, size.x, size.y};
    auto self = entitymgr_new<timetrigger_t>(em, pos, bounds, &timetrigger_type, id);
    self->target_id = props->get("trigger", -1);
    self->period = (float)props->get("period", -1) / 10.f;
}

// ----------------------------------------------------------------------------
//
//
//                                    DOOR
//
//
// ----------------------------------------------------------------------------

struct door_t : interactive_entity_t {
    anim_t anim;
    int other_id;
};

enum {
    // Animation ID
    DOOR_CLOSED,
    DOOR_PEEKED,
    DOOR_CLOSING,
};

static const uint16_t _door_frames_closed[] = {0};
static const uint16_t _door_frames_peeked[] = {1};
static const uint16_t _door_frames_closing[] = {4, 3, 2, 1, 0};
static const anim_seq_t door_anim_seqs[] = {
    {_door_frames_closed, NELEMS(_door_frames_closed), false},
    {_door_frames_peeked, NELEMS(_door_frames_peeked), true},
    {_door_frames_closing, NELEMS(_door_frames_closing), false},
};

void door_tick(door_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    if (anim_update(&self->anim, input->dt)) {
        anim_play(&self->anim, DOOR_CLOSED);
    }
    // draw light if door is open
    v2f p = self->pos;
    switch (self->anim.sequence) {
        case DOOR_PEEKED:
            light_push_capsule(ctx->lighting, p + v2f(11, 4), p + v2f(11, 12), 4, 0);
            break;
        case DOOR_CLOSING:
            light_push_capsule(ctx->lighting, p + v2f(11, 4), p + v2f(11, 12), 4 + self->anim.frame,
                               0);
            break;
    }
    render_push_sprite_w(&ctx->render, R_ID_DOOR, self->pos, Z_ENT_BACKROUND,
                         false, // flip
                         self->anim.frame);
}

void door_process_keys(door_t *self, game_state_t *game, input_state_t *input) {
    auto other_door = (entity_t *)entitymgr_find_by_id(&game->entitymgr, self->other_id);
    // opening - teleport to other door
    if (input->clicked(BUTTON_B_BIT)) {
        v2f ofs = other_door->pos - self->pos;
        game->player->pos += ofs;
        camera_set_pos(game->camera, game->player->pos);
        entity_send_event(other_door, EVENT_PLAYER_ENTERED);
    }
    // peeking - save peek state for game to render
    if (input->pressed(BUTTON_C_BIT)) {
        game->peekview = {true, other_door->pos - self->pos};
        self->hover_hidden = true;
        anim_play(&self->anim, DOOR_PEEKED);
    } else if (game->peekview.active) {
        game->peekview = {false};
        self->hover_hidden = false;
        anim_play(&self->anim, DOOR_CLOSED);
    }
}

void door_event(door_t *self, int event, void *param, void *ctx) {
    switch (event) {
        case EVENT_INTERACT:
            door_process_keys(self, (game_state_t *)ctx, (input_state_t *)param);
            break;
        case EVENT_PLAYER_ENTERED:
            anim_play(&self->anim, DOOR_CLOSING);
            break;
    }
}

static const interaction_t door_interactions[] = {
    {.asset = R_ID_MENUENTER, .anim_frames = 3},
    {.asset = R_ID_MENUPEEK, .anim_frames = 10},
    {-1},
};

DEF_ENTITY_TYPE_EXTRA(door, door_new, door_tick, door_event,
                      ENTITY_FLAG_COLLIDE | ENTITY_FLAG_INTERACTIVE, door_interactions);

void door_new(entitymgr_t *em, v2f pos, v2i size, struct entity_props_t *props, int id) {
    rect_t bounds = {0, 0, 13, 16};
    auto self = entitymgr_new<door_t>(em, pos, bounds, &door_type, id);
    anim_init_sequences(&self->anim, .06, door_anim_seqs);
    self->other_id = props->get("other");
}

// ----------------------------------------------------------------------------
//
//
//                                 SIDE DOOR
//
//
// ----------------------------------------------------------------------------

struct sidedoor_t : entity_t {
    bool open;
    anim_t anim;
};

enum {
    // Animation ID
    SIDEDOOR_OPEN,
    SIDEDOOR_CLOSED,
    SIDEDOOR_OPENING,
    SIDEDOOR_CLOSING,
};

static const uint16_t _sidedoor_frames_open[] = {3};
static const uint16_t _sidedoor_frames_closed[] = {0};
static const uint16_t _sidedoor_frames_opening[] = {0, 1, 2, 3};
static const uint16_t _sidedoor_frames_closing[] = {3, 2, 1, 0};
static const anim_seq_t sidedoor_anim_seqs[] = {
    {_sidedoor_frames_open, NELEMS(_sidedoor_frames_open), false},
    {_sidedoor_frames_closed, NELEMS(_sidedoor_frames_closed), false},
    {_sidedoor_frames_opening, NELEMS(_sidedoor_frames_opening), false},
    {_sidedoor_frames_closing, NELEMS(_sidedoor_frames_closing), false},
};

static void sidedoor_set_tiles(sidedoor_t *self, game_state_t *game) {
    // hacky way to make the door block entities and light - mutate the tilemap collisin layer
    int tx = self->pos.x / game->map.tilesize;
    int ty = self->pos.y / game->map.tilesize;
    for (int j = 0; j < 3; j++) {
        int tileidx = tx + (ty + j) * game->map.width;
        game->map.layers[2].data[tileidx] =
            self->open ? 0 : 132; // FIXME (arbitrary invisible solid tile)
    }
}

void sidedoor_tick(sidedoor_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    sidedoor_set_tiles(self, ctx);
    anim_update(&self->anim, input->dt);
    render_push_sprite_w(&ctx->render, R_ID_SIDEDOOR, self->pos, Z_ENT_BACKROUND,
                         false, // flip
                         self->anim.frame);
    light_push_capsule(ctx->lighting, self->pos + v2f(4, 4), self->pos + v2f(4, 20), 4, 0);
}

void sidedoor_event(sidedoor_t *self, int event, void *param, void *ctx) {
    auto game = (game_state_t *)ctx;
    switch (event) {
        case EVENT_TRIGGER:
            for (int i = 0; i < 4; i++) {
                v2f vel = v2f(10 * RANDFLOAT(-5.0f, 0.f), RANDFLOAT(-0.5f, 0.5f));
                spark_t spark(self->pos, vel, 0.8f, 7);
                game->particles->sparks.spawn(spark);
            }
            if (!self->open) {
                anim_play(&self->anim, SIDEDOOR_OPENING);
            } else {
                anim_play(&self->anim, SIDEDOOR_CLOSING);
            }
            self->open = !self->open;
            break;
    }
}

DEF_ENTITY_TYPE(sidedoor, sidedoor_new, sidedoor_tick, sidedoor_event, ENTITY_FLAG_COLLIDE);

void sidedoor_new(entitymgr_t *em, v2f pos, v2i size, struct entity_props_t *props, int id) {
    rect_t bounds = {0, 0, 8, 24};
    auto self = entitymgr_new<sidedoor_t>(em, pos, bounds, &sidedoor_type, id);
    anim_init_sequences(&self->anim, .06, sidedoor_anim_seqs);
    self->open = props->get("open", false);
    anim_play(&self->anim, self->open ? SIDEDOOR_OPEN : SIDEDOOR_CLOSED);
}

// ----------------------------------------------------------------------------
//
//
//                                   PANEL
//
//
// ----------------------------------------------------------------------------

enum panel_state_t {
    PANEL_IDLE = 0,
    PANEL_HACKING,
    PANEL_HACKED,
};

struct panel_t : interactive_entity_t {
    panel_state_t state;
    timeout_t idletime;
    float hacktime;
    anim_t anim;
    int target;
};

static const uint16_t _panel_frames_idle[] = {0, 1};
static const uint16_t _panel_frames_hacked[] = {2};
static const anim_seq_t panel_anim_seqs[] = {
    {_panel_frames_idle, NELEMS(_panel_frames_idle), true},
    {_panel_frames_hacked, NELEMS(_panel_frames_hacked), true},
};

void panel_tick(panel_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    if (self->state == PANEL_HACKING && timeout_expired(&self->idletime, input->dt, 0)) {
        self->state = PANEL_IDLE;
        self->hacktime = 0;
        self->hover_hidden = false;
    }
    if (self->state == PANEL_HACKING) {
        self->hacktime += input->dt;
        if (self->hacktime > 1.0f) {
            sound_play_note(&ctx->mixer, S_ID_HACK, SOUND_NOTE_C4, SND_VOLUME_SFX);
            self->state = PANEL_HACKED;
            anim_play(&self->anim, 1);
            entity_t *targ = entitymgr_find_by_id(em, self->target);
            if (targ) {
                entity_send_event(targ, EVENT_TRIGGER, self, ctx);
            }
            self->disabled = true;
        }
        int hackbar_frame = 0;
        if (self->hacktime > 0.75f) {
            hackbar_frame = 3;
        } else if (self->hacktime > 0.5f) {
            hackbar_frame = 2;
        } else if (self->hacktime > 0.25f) {
            hackbar_frame = 1;
        }
        render_push_sprite_w(&ctx->render, R_ID_HACKBAR, self->pos + v2f(2, -4), Z_ENT_BACKROUND,
                             false, hackbar_frame);
    }
    anim_update(&self->anim, input->dt);
    render_push_sprite_w(&ctx->render, R_ID_PANEL, self->pos, Z_ENT_BACKROUND, false,
                         self->anim.frame);
}

void panel_event(panel_t *self, int event, void *param, void *ctx) {
    auto input = (input_state_t *)param;
    if ((event == EVENT_INTERACT) && input->pressed(BUTTON_B_BIT) &&
        (self->state != PANEL_HACKED)) {
        self->state = PANEL_HACKING;
        self->hover_hidden = true;
        timeout_init(&self->idletime, 0.25);
    }
}

static const interaction_t panel_interactions[] = {
    {.asset = R_ID_MENUHACK, .anim_frames = 4},
    {-1},
};

DEF_ENTITY_TYPE_EXTRA(panel, panel_new, panel_tick, panel_event,
                      ENTITY_FLAG_COLLIDE | ENTITY_FLAG_INTERACTIVE, panel_interactions);

void panel_new(entitymgr_t *em, v2f pos, v2i size, struct entity_props_t *props, int id) {
    rect_t bounds = {0, 0, 16, 12};
    auto self = entitymgr_new<panel_t>(em,          // entitymgr_t
                                       pos,         // pos
                                       bounds,      // bounds
                                       &panel_type, // Magically defined type
                                       -1);         // ID
    self->state = PANEL_IDLE;
    self->target = props->get("target", -1);
    anim_init_sequences(&self->anim, 0.8, panel_anim_seqs);
}

// ----------------------------------------------------------------------------
//
//
//                                   SWITCH
//
//
// ----------------------------------------------------------------------------

struct switch_t : interactive_entity_t {

    bool on, oldstate;
    anim_t anim;
    int target;
    bool switched;
};

static const uint16_t _switch_frames_off[] = {1};
static const uint16_t _switch_frames_on[] = {0, 1};
static const anim_seq_t switch_anim_seqs[] = {
    {_switch_frames_off, NELEMS(_switch_frames_off), true},
    {_switch_frames_on, NELEMS(_switch_frames_on), true},
};

void switch_tick(switch_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    if (self->on != self->oldstate) {
        auto target = entitymgr_find_by_id(em, self->target);
        entity_send_event(target, EVENT_TRIGGER, self, ctx);
        self->oldstate = self->on;
    }
    anim_update(&self->anim, input->dt);
    render_push_sprite_w(&ctx->render, R_ID_SWITCH, self->pos, Z_ENT_BACKROUND, false,
                         self->anim.frame);
}

void switch_event(switch_t *self, int event, void *param, void *ctx) {
    auto input = (input_state_t *)param;
    if ((event == EVENT_INTERACT) && input->clicked(BUTTON_B_BIT)) {
        self->on = !self->on;
        anim_play(&self->anim, self->on);
    }
}

static const interaction_t switch_interactions[] = {
    {.asset = R_ID_MENUHACK, .anim_frames = 4},
    {-1},
};

DEF_ENTITY_TYPE_EXTRA(switch, switch_new, switch_tick, switch_event,
                      ENTITY_FLAG_COLLIDE | ENTITY_FLAG_INTERACTIVE, switch_interactions);

void switch_new(entitymgr_t *em, v2f pos, v2i size, struct entity_props_t *props, int id) {
    rect_t bounds = {0, 0, 8, 8};
    auto self = entitymgr_new<switch_t>(em, pos, bounds, &switch_type, -1);
    self->target = props->get("target", -1);
    anim_init_sequences(&self->anim, 0.8, switch_anim_seqs);
}

// ----------------------------------------------------------------------------
//
//
//                                    EXIT
//
//
// ----------------------------------------------------------------------------

struct exit_line_t : particle_t {
    float t, l;

    exit_line_t(int _x, int _y) {
        x = _x;
        y = _y;
        t = 0;
        l = 3;
    }

    void tick(float dt, void *ctx) {
        t += dt;
        y -= 30 * dt;
        l += 1.2f;
        game_state_t *gs = (game_state_t *)ctx;
        render_push_line_w(&gs->render, x, y, x, y - l, Z_ENT_BACKROUND, 5);
        light_push_point(gs->lighting, v2f(x, y), 4);
        if (t > 1.5f) {
            alive = false;
        }
    }
};

struct exit_t : entity_t {
    emitter_t<exit_line_t, 16> particles;
    timeout_t emit;
    float t;
};

void exit_tick(exit_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {

    rect_t bounds = entity_bounds(self).shrink2(12, 12);
    FOREACH_ENTITY_IN_BOUNDS(e, em, bounds, ENTITY_FLAG_PLAYER) {
        entity_send_event(e, EVENT_EXIT, self, ctx);
    }

    float alpha = sinf(2 * self->t);

    auto p = self->pos + v2f(16, 24 + 3 * alpha);

    if (timeout_expired(&self->emit, input->dt, 0)) {
        timeout_init(&self->emit, RANDFLOAT(0.1f, 0.3f));
        auto pp = p + v2f(RANDINT(-10, 10), 20);
        self->particles.spawn(pp.x, pp.y);
    }

    self->particles.tick(input->dt, ctx);

    self->t += input->dt;
    render_push_rect_w(&ctx->render, p.x - 16, p.y - 24, p.x + 16, p.y + 24, Z_ENT_PLAYER, 5);
    render_push_rect_w(&ctx->render, p.x - 14, p.y - 22, p.x + 14, p.y + 22, Z_ENT_PLAYER, 6);
    render_push_rect_w(&ctx->render, p.x - 12, p.y - 20, p.x + 12, p.y + 20, Z_ENT_PLAYER, 7);

    auto ps = self->pos + v2f(10, 12 + 3.5f * alpha);
    render_push_sprite_w(&ctx->render, R_ID_EXIT, ps, Z_ENT_BG3, false, 0);

    light_push_capsule(ctx->lighting, p + v2f(0, -8), p + v2f(0, 8), 18 + alpha * 2, 0);
}

DEF_ENTITY_TYPE(exit, exit_new, exit_tick, NULL, ENTITY_FLAG_COLLIDE);

void exit_new(entitymgr_t *em, v2f pos, v2i size, struct entity_props_t *props, int id) {
    rect_t bounds = {0, 0, 32, 48};
    auto self = entitymgr_new<exit_t>(em,         // entitymgr_t
                                      pos,        // pos
                                      bounds,     // bounds
                                      &exit_type, // Magically defined type
                                      id);        // ID
    self->t = 0;
}

// ----------------------------------------------------------------------------
//
//
//                                   MOVER
//
//
// ----------------------------------------------------------------------------

struct mover_t : entity_t {
    int target_id;
    v2f target_ofs;
    v2f cur_pos;
    bool first;
    float vel;
};

void mover_tick(mover_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    rect_t ebounds = entity_bounds(self);
    auto target = entitymgr_find_by_id(em, self->target_id);

    if (self->first) {
        self->target_ofs = target->pos - self->pos;
        self->first = false;
    }

    if (self->bounds.w == 8) {
        // vertical
        float newy = self->cur_pos.y + self->vel * input->dt;
        if (newy >= (ebounds.y + ebounds.h - target->bounds.h)) {
            self->vel = -self->vel;
            newy = (ebounds.y + ebounds.h - target->bounds.h);
        }
        if (newy <= ebounds.y) {
            self->vel = -self->vel;
            newy = ebounds.y;
        }

        self->cur_pos.y = newy;
        target->pos = self->cur_pos + self->target_ofs;
    } else {
        // horizontal
        float newx = self->cur_pos.x + self->vel * input->dt;
        if (newx >= (ebounds.x + ebounds.w - target->bounds.w)) {
            self->vel = -self->vel;
            newx = (ebounds.x + ebounds.w - target->bounds.w);
        }
        if (newx <= ebounds.x) {
            self->vel = -self->vel;
            newx = ebounds.x;
        }

        self->cur_pos.x = newx;
        target->pos = self->cur_pos + self->target_ofs;
    }
}

DEF_ENTITY_TYPE(mover, mover_new, mover_tick, NULL, ENTITY_FLAG_COLLIDE);

void mover_new(entitymgr_t *em, v2f pos, v2i size, struct entity_props_t *props, int id) {
    rect_t bounds = {0, 0, size.x, size.y};
    auto self = entitymgr_new<mover_t>(em, pos, bounds, &mover_type, id);
    self->target_id = props->get("target", -1);
    self->vel = props->get("velocity", 30);
    self->cur_pos = pos;
    self->first = true;
}

// ----------------------------------------------------------------------------
//
//
//                                   GRATE
//
//
// ----------------------------------------------------------------------------

struct grate_t : entity_t {
    anim_t anim;
    float glow;
};

static const uint16_t grate_frames[] = {0, 1, 2, 3};
void grate_tick(grate_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {

    rect_t bounds = entity_bounds(self);
    bounds.y -= 4;

    bool found = false;
    FOREACH_ENTITY_IN_BOUNDS(e, em, bounds, ENTITY_FLAG_PLAYER) {
        found = true;
    }
    if (found) {
        if (self->glow < 6) {
            self->glow += 30.f * input->dt;
        }
    } else {
        if (self->glow > 0) {
            self->glow -= 50.f * input->dt;
        }
    }

    if (self->glow > 1) {
        light_push_point(ctx->lighting, self->pos + v2f{4, 0}, self->glow);
        render_push_sprite_w(&ctx->render, R_ID_DOWNARROW, self->pos + v2f{2, -17}, Z_ENT_BACKROUND,
                             false, // flip
                             self->anim.frame);
    }
    anim_update(&self->anim, input->dt);
}

DEF_ENTITY_TYPE(grate, grate_new, grate_tick, NULL, ENTITY_FLAG_COLLIDE);

void grate_new(entitymgr_t *em, v2f pos, v2i size, struct entity_props_t *props, int id) {
    rect_t bounds = {0, 0, size.x, size.y};
    auto self = entitymgr_new<grate_t>(em, pos, bounds, &grate_type, id);
    anim_init_sequence(&self->anim, 0.2, grate_frames, 4, true);
}

// ----------------------------------------------------------------------------
//
//
//                                   GOAL
//
//
// ----------------------------------------------------------------------------

struct goal_t : interactive_entity_t {
    anim_t anim;
    float t;
};

static const uint16_t _goal_frames_idle[] = {0};
static const anim_seq_t goal_anim_seqs[] = {
    {_goal_frames_idle, NELEMS(_goal_frames_idle), true},
};

void goal_tick(goal_t *self, entitymgr_t *em, input_state_t *input, game_state_t *ctx) {
    anim_update(&self->anim, input->dt);
    self->t += input->dt * 5;
    auto dy = sin(self->t) * 2;
    render_push_sprite_w(&ctx->render, R_ID_FILE, self->pos + v2f{0, dy}, Z_ENT_ENEMY, false,
                         self->anim.frame);
}

void goal_event(goal_t *self, int event, void *param, void *ctx) {
    auto input = (input_state_t *)param;
    if ((event == EVENT_INTERACT) && input->clicked(BUTTON_B_BIT)) {
        printf("goal activated\n");
    }
}

static const interaction_t goal_interactions[] = {
    {.asset = R_ID_MENUHACK, .anim_frames = 4},
    {-1},
};

DEF_ENTITY_TYPE_EXTRA(goal, goal_new, goal_tick, goal_event,
                      ENTITY_FLAG_COLLIDE | ENTITY_FLAG_INTERACTIVE, goal_interactions);

void goal_new(entitymgr_t *em, v2f pos, v2i size, struct entity_props_t *props, int id) {
    rect_t bounds = {0, 0, 16, 16};
    auto self = entitymgr_new<goal_t>(em, pos, bounds, &goal_type, -1);
    anim_init_sequences(&self->anim, 0.8, goal_anim_seqs);
}
