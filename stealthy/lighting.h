#include <cstring>
#include "engine/common.h"
#include "game.h"

static const v2f NORM_N = {0, -1};
static const v2f NORM_S = {0, 1};
static const v2f NORM_E = {1, 0};
static const v2f NORM_W = {-1, 0};

#define LIGHT_DEBUG_WALL_NORM (0)

#define RADS(DEG) (float)((float)M_PI * (float)(DEG) / 180.f)
struct wall_def_t {
    v2f s, e, n;
};

enum {
    LIGHT_TYPE_CAPSULE,
    LIGHT_TYPE_POINT,
    LIGHT_TYPE_SPOT,
};

enum {
    SHADE_LEVELS = 5,
    MAX_DEFS = 64,
    MAX_WALLS = 128,
};

struct light_def_t {
    int type;
    int tint;
    union {
        struct {
            v2f pos;
            float radius;
        } p;
        struct {
            v2f pos;
            float radius;
            float angle, spread;
        } s;
        struct {
            v2f pa, pb;
            float radius;
        } c;
    };
};

struct lighting_t {
    int def_count;
    collide_conf_t collide;
    light_def_t defs[MAX_DEFS];
    rect_t view_bounds, view_cur;
    uint8_t tintmap[GAME_RES_WIDTH * GAME_RES_HEIGHT];
    float lightmap_prev[GAME_RES_WIDTH * GAME_RES_HEIGHT];
    float lightmap[GAME_RES_WIDTH * GAME_RES_HEIGHT];
    uint8_t raymap[GAME_RES_WIDTH * GAME_RES_HEIGHT];
    wall_def_t walls[MAX_WALLS];
    size_t walls_count;
};

static rect_t light_def_bounds(light_def_t &d) {
    float minx, miny, maxx, maxy, w, h;
    switch (d.type) {
        case LIGHT_TYPE_POINT:
            w = h = d.p.radius * 2;
            return rect_t(d.p.pos.x - d.p.radius, d.p.pos.y - d.p.radius, w, h);
        case LIGHT_TYPE_SPOT:
            // FIXME
            w = h = d.p.radius * 2;
            return rect_t(d.p.pos.x - d.p.radius, d.p.pos.y - d.p.radius, w, h);
        case LIGHT_TYPE_CAPSULE:
            minx = MIN(d.c.pa.x, d.c.pb.x);
            maxx = MAX(d.c.pa.x, d.c.pb.x);
            miny = MIN(d.c.pa.y, d.c.pb.y);
            maxy = MAX(d.c.pa.y, d.c.pb.y);
            w = maxx - minx + 2 * d.c.radius;
            h = maxy - miny + 2 * d.c.radius;
            return rect_t(minx - d.c.radius, miny - d.c.radius, w, h);
    }
    ASSERT(false);
}

static void light_push_point(lighting_t &lt, v2f pos, float radius) {
    ASSERT(lt.def_count < MAX_DEFS);
    light_def_t &d = lt.defs[lt.def_count];
    d.type = LIGHT_TYPE_POINT;
    d.p.pos = pos;
    d.p.radius = radius;
    if (light_def_bounds(d).overlap(lt.view_bounds)) {
        lt.def_count++;
    }
}

static void light_push_spot(lighting_t &lt, v2f pos, float radius, float spread, float angle,
                            int colofs) {
    ASSERT(lt.def_count < MAX_DEFS);
    light_def_t &d = lt.defs[lt.def_count];
    d.type = LIGHT_TYPE_SPOT;
    d.s.pos = pos;
    d.s.radius = radius;
    d.s.spread = spread;
    d.s.angle = angle;
    d.tint = colofs;
    if (light_def_bounds(d).overlap(lt.view_bounds)) {
        lt.def_count++;
    }
}

static void light_push_capsule(lighting_t &lt, v2f pa, v2f pb, float radius, int colofs) {
    ASSERT(lt.def_count < MAX_DEFS);
    light_def_t &d = lt.defs[lt.def_count];
    d.type = LIGHT_TYPE_CAPSULE;
    d.c.pa = pa;
    d.c.pb = pb;
    d.c.radius = radius;
    d.tint = colofs;
    if (light_def_bounds(d).overlap(lt.view_bounds)) {
        lt.def_count++;
    }
}

static void light_build_map_local(lighting_t &lt, collide_conf_t &coll, rect_t tilerange) {
    tilemap_t *map = (tilemap_t *)coll.context;

#define SOLID(x, y) ((coll.tile(map, x, y) == COLLIDE_TILE_IMPASSABLE))

    lt.walls_count = 0;

    int tsx = tilerange.x;
    int tex = tilerange.x + tilerange.w;
    int tsy = tilerange.y;
    int tey = tilerange.y + tilerange.h;
    // note we lower the walls for all top-facing edges such that the lighting system
    // lights the top of walkable surfaces.

    // south facing
    for (int j = tsy; j < tey; j++) {
        int last = tsx;
        for (int i = tsx; i < tex; i++) {
            if (SOLID(i, j)) {
                if ((j < tey - 1) && (SOLID(i, j + 1) || (i == tex - 1))) {
                    if ((i - last) > 0) {
                        lt.walls[lt.walls_count++] = {v2f(last, j), v2f(i, j), NORM_S};
                    }
                    last = i + 1;
                }
            } else {
                if ((i - last) > 0) {
                    lt.walls[lt.walls_count++] = {v2f(last, j), v2f(i, j), NORM_S};
                }
                last = i + 1;
            }
        }
    }
    // north facing
    for (int j = tsy; j < tey; j++) {
        int last = tsx;
        for (int i = tsx; i < tex; i++) {
            if (SOLID(i, j)) {
                if ((j > tsy) && (SOLID(i, j - 1) || (i == tex - 1))) {
                    if ((i - last) > 0) {
                        lt.walls[lt.walls_count++] = {v2f(last, j - 1 + .5f), v2f(i, j - 1 + .5f),
                                                      NORM_N};
                    }
                    last = i + 1;
                }
            } else {
                if ((i - last) > 0) {
                    lt.walls[lt.walls_count++] = {v2f(last, j - 1 + .5f), v2f(i, j - 1 + .5f),
                                                  NORM_N};
                }
                last = i + 1;
            }
        }
    }
    // east facing
    for (int i = tsx; i < tex; i++) {
        int last = tsy;
        for (int j = tsy; j < tey; j++) {
            bool add = false;
            if ((SOLID(i, j)) && ((i < tex - 1) && (SOLID(i + 1, j) || (j == tey - 1)))) {
                add = true; // t junction
            } else if (!SOLID(i, j)) {
                add = true; // end of vertical wall
            }
            if (add) {
                // lower top if horizontal wall to left, lower bot if horizontal wall to right
                float topofs = SOLID(i + 1, last - 1) ? 0 : .5f;
                float botofs = SOLID(i + 1, j) ? .5f : 0;
                if ((j - last) > 0) {
                    lt.walls[lt.walls_count++] = {v2f(i + 1, last - 1 + topofs),
                                                  v2f(i + 1, j - 1 + botofs), NORM_E};
                }
                last = j + 1;
            }
        }
    }
    // west facing
    for (int i = tsx; i < tex; i++) {
        int last = tsy;
        for (int j = tsy; j < tey; j++) {
            bool add = false;
            if ((SOLID(i, j)) && ((i > tsx) && (SOLID(i - 1, j) || (j == tey - 1)))) {
                add = true; // t junction
            } else if (!SOLID(i, j)) {
                add = true; // end of vertical wall
            }
            if (add) {
                // lower top if horizontal wall to right, lower bot if horizontal wall to left
                float topofs = SOLID(i, last - 1) ? 0 : .5f;
                float botofs = SOLID(i, j) ? .5f : 0;
                if ((j - last) > 0) {
                    lt.walls[lt.walls_count++] = {v2f(i, last - 1 + topofs), v2f(i, j - 1 + botofs),
                                                  NORM_W};
                }
                last = j + 1;
            }
        }
    }
}

static void light_clear(lighting_t &lt, collide_conf_t collide, rect_t &old_view) {
    lt.def_count = 0;
    lt.view_bounds = old_view;
    lt.collide = collide;
    for (int i = 0; i < GAME_RES_WIDTH * GAME_RES_HEIGHT; i++) {
        lt.lightmap_prev[i] = lt.lightmap[i];
        lt.lightmap[i] = 0.0f;
        lt.tintmap[i] = 0;
    }
}

static void light_view(lighting_t &lt, rect_t &view) {
    lt.view_cur = view;
}

static float sqr(float x) {
    return x * x;
}

static float dist2(v2f v, v2f w) {
    return sqr(v.x - w.x) + sqr(v.y - w.y);
}

static float dist_to_seg(v2f p, v2f v, v2f w) {
    float l2 = dist2(v, w);

    if (l2 == 0)
        return dist2(p, v);
    float t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2;
    t = CLAMP(t, 0, 1);

    v2f proj = v + (w - v) * t;
    return (p - proj).len();
}

static void light_draw_capsule(lighting_t &lt, rect_t &b, v2f pa, v2f pb, float r, int tint) {
    int minx = CLAMP(b.x, 0, 64);
    int maxx = CLAMP(b.x + b.w, 0, 64);
    int miny = CLAMP(b.y, 0, 64);
    int maxy = CLAMP(b.y + b.h, 0, 64);
    for (int j = miny; j < maxy; j++) {
        for (int i = minx; i < maxx; i++) {
            int px = i + j * GAME_RES_WIDTH;
            float v = lt.lightmap[px];

            float d = dist_to_seg(v2f(i + .5, j + .5), pa, pb);
            float dv = CLAMP((1.0f - (d / r)), 0, 1);
            v += dv;

            lt.lightmap[px] = MIN(v, 1.f);
            lt.tintmap[px] = MAX((uint8_t)tint, lt.tintmap[px]);
        }
    }
}

static void light_draw_point(lighting_t &lt, rect_t &b, v2f p, float r) {
    int minx = CLAMP(b.x, 0, 64);
    int maxx = CLAMP(b.x + b.w, 0, 64);
    int miny = CLAMP(b.y, 0, 64);
    int maxy = CLAMP(b.y + b.h, 0, 64);
    for (int j = miny; j < maxy; j++) {
        int xofs = 0; // RANDINT(-1, 1); // noise in the X axis on the light ring
        for (int i = minx; i < maxx; i++) {
            int px = i + j * GAME_RES_WIDTH;
            if (lt.raymap[px] == 0) {
                continue;
            }
            float v = lt.lightmap[px];

            float d = (v2f(i + xofs + .5, j + .5) - p).len();
            float dv = CLAMP((1.0f - (d / r)), 0, 1);
            v += dv;

            lt.lightmap[px] = MIN(v, 1.f);
        }
    }
}

static void light_draw_spot(lighting_t &lt, rect_t &b, v2f p, float r, float s, float a, int tint) {

    v2f p0 = p;
    v2f p1 = p + v2f(cos(a - s / 2) * r, sin(a - s / 2) * r);
    v2f p2 = p + v2f(cos(a + s / 2) * r, sin(a + s / 2) * r);

#define SPOT_PUT(x, y, px)                                 \
    {                                                      \
        if (lt.raymap[px] != 0) {                          \
            if (tint) {                                    \
                lt.tintmap[px] = tint;                     \
            } else {                                       \
                float v = lt.lightmap[px];                 \
                float d = (v2f(x + .5, y + .5) - p).len(); \
                float dv = CLAMP((1.0f - (d / r)), 0, 1);  \
                v += dv;                                   \
                lt.lightmap[px] = MIN(v, 1.f);             \
            }                                              \
        }                                                  \
    }

    int x0 = p0.x;
    int y0 = p0.y;
    int x1 = p1.x;
    int y1 = p1.y;
    int x2 = p2.x;
    int y2 = p2.y;

    int width = GAME_RES_WIDTH;
    int height = GAME_RES_HEIGHT;
    // sort the points vertically
    if (y1 > y2) {
        SWAP(x1, x2);
        SWAP(y1, y2);
    }
    if (y0 > y1) {
        SWAP(x0, x1);
        SWAP(y0, y1);
    }
    if (y1 > y2) {
        SWAP(x1, x2);
        SWAP(y1, y2);
    }

    float dx_far = (float)(x2 - x0) / (y2 - y0 + 1);
    float dx_upper = (float)(x1 - x0) / (y1 - y0 + 1);
    float dx_low = (float)(x2 - x1) / (y2 - y1 + 1);
    float xf = x0;
    float xt = x0 + dx_upper; // if y0 == y1, special case
    for (int y = y0; y <= (y2 > height - 1 ? height - 1 : y2); y++) {
        if (y >= 0) {
            for (int x = (xf > 0 ? (int)(xf) : 0); x <= (xt < width ? xt : width - 1); x++) {
                int idx = x + y * width;
                SPOT_PUT(x, y, idx);
            }
            for (int x = (xf < width ? (int)(xf) : width - 1); x >= (xt > 0 ? xt : 0); x--) {
                int idx = x + y * width;
                SPOT_PUT(x, y, idx);
            }
        }
        xf += dx_far;
        if (y < y1)
            xt += dx_upper;
        else
            xt += dx_low;
    }
}

static uint8_t palshade(bitmap_t &lookup, int col, uint8_t shade) {
    int lookup_x = col;
    int lookup_y = shade;
    return *PIXEL_PTR(&lookup, lookup_x, lookup_y, 1);
}

static float dropoff(float v) {
    return v * v;
}

static void light_draw_shadow_quad(lighting_t &lt, v2f pts[]) {
    bitmap_t raymap = BITMAP8(GAME_RES_WIDTH, GAME_RES_HEIGHT, lt.raymap);
    bitmap_draw_filltri(&raymap, pts[0].x, pts[0].y, pts[1].x, pts[1].y, pts[2].x, pts[2].y, 0);
    bitmap_draw_filltri(&raymap, pts[0].x, pts[0].y, pts[2].x, pts[2].y, pts[3].x, pts[3].y, 0);
}

static void light_shadow_cast(lighting_t &lt, bitmap_t &fb, v2f pos, int light_range) {
    memset(lt.raymap, 1, sizeof(lt.raymap));

    for (size_t i = 0; i < lt.walls_count; i++) {
        const wall_def_t &w = lt.walls[i];

        v2f wall_start = ((v2f)w.s + v2f(0, 1)) * (float)TILE_SIZE_PX - v2f(0, 1);
        v2f wall_end = ((v2f)w.e + v2f(0, 1)) * (float)TILE_SIZE_PX - v2f(0, 1);
        v2f norm = v2f(-w.n.x, -w.n.y);

        float wall_dot = norm.dot(pos - wall_start);
        bool visible = wall_dot < 0;

        if (LIGHT_DEBUG_WALL_NORM) {
            // Wall normal debug
            int x0 = wall_start.x - lt.view_cur.x;
            int y0 = wall_start.y - lt.view_cur.y;
            int x1 = wall_end.x - lt.view_cur.x;
            int y1 = wall_end.y - lt.view_cur.y;
            bitmap_draw_line(&fb, x0, y0, x1, y1, visible ? 14 : 13);
            int midx = (x0 + x1) / 2;
            int midy = (y0 + y1) / 2;
            bitmap_draw_line(&fb, midx, midy, midx + 2 * norm.x, midy + 2 * norm.y,
                             visible ? 11 : 10);
        }

        v2f ds = wall_start - pos;
        v2f de = wall_end - pos;

        if ((ds.len() > light_range && de.len() > light_range) || !visible) {
            continue;
        }

        v2f start_project = pos + ds * light_range;
        v2f end_project = pos + de * light_range;

        v2f ofs = v2f(lt.view_cur.x, lt.view_cur.y);
        v2f pts[] = {wall_start - ofs, wall_end - ofs, end_project - ofs, start_project - ofs};
        light_draw_shadow_quad(lt, pts);
    }
}

static float light_probe(lighting_t &lt, v2f pos) {
    v2f p = pos - v2f(lt.view_cur.x, lt.view_cur.y);
    if ((p.x >= 0) && (p.x < GAME_RES_WIDTH) && (p.y >= 0) && (p.y < GAME_RES_HEIGHT)) {
        int px = ((int)p.x) + ((int)p.y) * GAME_RES_WIDTH;
        return lt.lightmap_prev[px];
    }
    return 0;
}

static void light_render(lighting_t &lt, bitmap_t &fb, rect_t &view, bitmap_t &tileset, v2f pos,
                         collide_conf_t &collide) {

    int tx = MAX(lt.view_cur.x / 8 - 4, 0);
    int ty = MAX(lt.view_cur.y / 8 - 4, 0);
    int tw = 16;
    int th = 16;

    // BUILD LOCAL STATE
    // -------------------------------------------------------------------------
    rect_t tilerange = rect_t(tx, ty, tw, th);
    light_build_map_local(lt, collide, tilerange);

    // LIGHT SOURCES
    // -----------------------------------------------------------------------------
    for (int i = 0; i < lt.def_count; i++) {
        light_def_t &d = lt.defs[i];
        rect_t bv = light_def_bounds(d).translate(-view.x, -view.y);

        switch (d.type) {
            case LIGHT_TYPE_POINT:
                light_shadow_cast(lt, fb, d.p.pos, d.p.radius);
                light_draw_point(lt, bv, d.p.pos - v2f(view.x, view.y), d.p.radius);
                break;
            case LIGHT_TYPE_SPOT:
                light_shadow_cast(lt, fb, d.s.pos, d.s.radius);
                light_draw_spot(lt, bv, d.s.pos - v2f(view.x, view.y), d.s.radius, d.s.spread,
                                d.s.angle, d.tint);
                break;
            case LIGHT_TYPE_CAPSULE:
                light_draw_capsule(lt, bv, d.c.pa - v2f(view.x, view.y),
                                   d.c.pb - v2f(view.x, view.y), d.c.radius, d.tint);
                break;
            default:
                abort();
        }
    }

    /*
    // FORWARD FACING SHADING
    -------------------------------------------------------------------- for (int
    j = 0; j < th; j++) { for (int i = 0; i < tw; i++) { if
    (collide.tile(collide.context, tx + i, ty + j) == 42) { //FIXME:
    COLLIDE_TILE_FRONT_FACING) { int dy = pos.y - ((ty + j) * TILE_SIZE_PX + 4);
                // if this tile is forward facing and in front of the player,
    start to darken it int darken = dy < 0 ? MIN(SHADE_LEVELS - 1, -dy / 12) : 0;
                // darken the whole tile
                if (darken) {
                    int px = (tx + i) * TILE_SIZE_PX - lt.view_cur.x;
                    int py = (ty + j) * TILE_SIZE_PX - lt.view_cur.y;

                    int startx = MAX(px, 0);
                    int starty = MAX(py, 0);
                    int endx = MIN(px + TILE_SIZE_PX, 64);
                    int endy = MIN(py + TILE_SIZE_PX, 64);

                    for (int x = startx; x < endx; x++) {
                        for (int y = starty; y < endy; y++) {
                            float v = lt.lightmap[x + y * GAME_RES_WIDTH];
                            v -= darken * 0.2f;
                            lt.lightmap[x + y * GAME_RES_WIDTH] = CLAMP(v, 0, 1);
                        }
                    }
                }
            }
        }
    }
    */

    // RENDER SHADING
    // ----------------------------------------------------------------------------
    for (int j = 0; j < GAME_RES_HEIGHT; j++) {
        for (int i = 0; i < GAME_RES_WIDTH; i++) {
            uint8_t tint = lt.tintmap[i + j * GAME_RES_WIDTH];
            uint8_t shade = (int)((SHADE_LEVELS)*dropoff(1 - lt.lightmap[i + j * GAME_RES_WIDTH]));
            uint8_t orig_pix = *PIXEL_PTR(&fb, i, j, 1);
            *PIXEL_PTR(&fb, i, j, 1) = tint + palshade(tileset, orig_pix, shade);

            // ray debug
            if (0) {
                if (lt.raymap[i + j * GAME_RES_WIDTH]) {
                    *PIXEL_PTR(&fb, i, j, 1) = 14; // 48 + 5;
                }
            }
        }
    }
}
