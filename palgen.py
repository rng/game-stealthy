from PIL import Image

def main():

    img = Image.open("stealthy/colmap.png")
    img = img.convert('RGBA')
    w, h = img.size
    data = img.load()

    print("GIMP Palette")
    print("#Palette Name: Stealthy")
    print("#Description:")
    print("#Colors: %d" % (w * h))
    for j in range(h):
        for i in range(w):
            r,g,b,_ = data[i,j]
            print("%3d %3d %3d %02x%02x%02x" % (r,g,b,r,g,b))

main()
