# Stealthy Notes

## Tasks

- [ ] Add hiding logic -> auto hide behind things on the second layer? Don't love. The aggro meter is slow enough that the character never really needs to hide. Does camera alert enemies?
- [ ] visual sound effects to show enemy walking by outside.
- [ ] Clean up peeking visualization
- [ ] Add more door designs so players know which door leads to which / help them keep track of where they are.
- [ ] choose game name
- [ ] Design itch page
- [ ] Design game logo and cover art

### Gameplay

- [ ] prototype locking enemies in rooms logic
- [ ] camera pan to unlocked door
- [ ] temporary disable of enemy mechanic (ie EMP/smoke bomb)?
- [ ] Need a more obvious death animation that shows players how they died. E.g. in shooters, the person who killed you is highlighted in red. or the short period death is replayed on death.
- [ ] Need immediate feedback on pressing the down button (don't add delay between pressing down and showing the area below the player. OR display some kind of loading bar if there is a purpose to the delay.)
- [ ] Make hole in ground more obvious. I still haven't figured out how to enter the lower area.
- [ ] add in enemies i made art for
- [ ] Design level with new tile set.

## Bugs

- [ ] Everything is too visually chaotic right now, Doesn't feel stealthy.
- [ ] Game pacing issue: can rush through levels w/o issue. Need to have a reason for players to peek / look around them.
- [ ] collision with the floating floor tiles look odd. Fix collision box.
- [ ] Don't recolor light as a loading bar on detection - change enemy sprite instead. e.g. same enemy idea [here](https://youtu.be/V9NrqsSFFh4?t=1485)
- [ ] Recolor colored lighting -> the additive colors are too harsh right now.
- [x] Jump feels too floaty atm
- [ ] Scarf animation is backwards atm - reverse frames. Remove scarf bc its distracting.
- [ ] ceiling lights have flag 2 set from an unknown source...
- [ ] Recolor pupil in peek icon to be more legible.
- [ ] players can dramatically change their trajectory while falling. reduced that a little
- [ ] camera to not track player when player is just moving in center of screen
- [ ] run animation of player should NOT look at the screen, confusing about what direction they are running
- [ ] (NIT) Laser light color should be whiter if it's emitting light
- [ ] enemy obstacle that shoots needs to telegraph attack better.
- [ ] Laser + gun relationship needs to be more clear (e.g. in Portal, it draws a line between an element that triggers another)
- [ ] Enemy always faces right when standing at a waypoint

---

# Done

- [x] Allow player to jump through thin tiles.
- [x] Watch more eastward to figure out how they do lighting.
- [x] write out list of animations we can make
- [x] Figure out game play elements - copy stealth bastards.
- [x] Don't allow jumping on ladder. this allows players to jump extra high by using the ladder as a to project themselves off of.
- [x] enter room
- [x] Enemy AI
- [x] Enemy alerting (one enemy should alert others when they see the player)
- [x] player enter room, player leave room. On enter, shade the outside environment to a dark color. Player needs to explicitly leave room.
- [x] Redesign enemy robot (no legs)

### Door Logic
- [x] Interact with door to open menu
- [x] (Won't do) Navigate menu
- [x] send button press to door.
- [x] quit from menu
- [x] (Won't do) render new map on top of current map on entering room.

---

# Discarded

## Stealth mechanic ideas

* Scroll screen to scope enemies - you are vulnerable during this though.
* No killing guards
* goal of sorts to end a level (door, thing to steal)
* Camera w/ vision cone, rotates around pivot.
* Button to toggle lights
* laser for instant death?
* goal unlock mechanic. Needs to be hit before you can finish a level.
* peek/scan through door

Instant death vs aggro level?
I like the latter more.
e.g. security levels until game over.
