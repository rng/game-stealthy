# LowRezJam 2023

## Tasks
P0
- [ ] alert states (I, II, III). Cameras need to alert robots and then robots should look for you
- [ ] player standing in front of laser glitches player sprite and laser.
- [ ] animate the splash art
- [ ] animation along the lines to show which thing was affected

P1
- [ ] goal of each level should be "collect some item"
- [ ] Build a ton of levels
- [ ] Screen explaining the controls
- [ ] doors should be a brighter color to catch the player's eye
- [ ] animate background

P2
- [ ] fix door despawning and lighting

P4
- [ ] different enemies
- [ ] more interactive things

## August 13, 2023
- [x] redraw main menu
- [x] itch page
- [x] credits page
- [x] push to itch.io
- [x] disable the debug stats
- [x] 1-2 levels
- [ ] transition between levels

Uploading to Itch
- Install emscripten https://emscripten.org/docs/getting_started/downloads.html
- Install butler https://itch.io/docs/butler/
    Right click and open from Finder since the app is not signed.
- ITCHNAME=microaeris/blue-stealth PLAT=web UNIFIED_BUILD=1 make itchupload

## August 11, 2023
- [x] add jump and climb anim
- [x] draw jump animation
- [x] draw climbing animation
- [x] Added bgm https://pixabay.com/music/beats-stealth-escape-3937/
- [x] sound effects
- [x] more contrast on darker colors

Door sfx
couldn't get it to work :/
Game crashes w/ ASAN error when passing ctx's mixer to play the sound.

Ended up using these sound effects:
https://kronbits.itch.io/freesfx

Enemy shoot
https://www.storyblocks.com/audio/stock/fat-laser-cannon-hefvl22lprk0wxsh8u.html

Shoot
https://www.storyblocks.com/audio/stock/laser-gun-single-shot-blrxnvtn8psk0wxxgd4.html

Door open
https://www.storyblocks.com/audio/stock/automatic-door-unlock-slide-open-rg18f0hn8vhk0wxvcgd.html

Jump land
https://www.storyblocks.com/audio/stock/footsteps-jump-on-wood-sltvfc3hudsk0wxttjf.html

Jump up
https://www.storyblocks.com/audio/stock/8-bit-hit-9-blvlp5rn8dhk0wy4wlp.html
https://www.storyblocks.com/audio/stock/8-bit-crash-5-hgrvp50hlvhk0wy4wof.html

Game over
https://www.storyblocks.com/audio/stock/video-game-game-over-3-hemeckr28vbk0wy4mel.html


## August 8, 2023
- [x] walking enemies to cause damage (touch or shooty damage)
- [x] [won't do] hide enemy cone
- [x] fix scarf animation, it's waving in the wrong direction

`__attribute__((optnone))` for clang to not optimize a function

## August 6, 2023
- [x] logo animation
- [x] [Ross] title screen
- [x] Splash art
- [x] name
- [x] logo

## August 2-4, 2023
- [x] write tutorial on controls
- [x] Write tasks to make movement feel good
- [x] Camera shouldn't follow player when player is in center of screen - copy fez
- [x] have to press up to grab on to the ladder
- [x] [Won't do] map jump to spacebar
- [x] add more deceleration to the jump
- [x] during jump, character stutters in the air oddly. Also seen while climbing ladder. When the camera moves...
- [x] Camera stutters on look

LR arrows: Move
Z: jump
X: interact
C: peek
Up, Down Arrows: Look in dir
Press and hold up/down: look up/down
up: climb ladder
z+down: drop through platform

Camera window: https://www.gamedeveloper.com/design/scroll-back-the-theory-and-practice-of-cameras-in-side-scrollers

Z: Jump
LR arrows: Move
Up, Down Arrows: Look
Z+Down: Drop through platform

# Map making

- Need some buffer on the side of the level for paralax to look good


- Entity notes:
  - light:
    - params:
      - bright: how far the light reaches in pixels
      - angle: facing direction of spotlight in degrees (90 = down)
      - width: spread of spotlight in degrees
  - panel:
    - params:
      - target: id of object to trigger when panel is hacked
  - enemy:
    - params:
      -
  - trigger:
    - behaviour: on player contact, trigger target entity
    - params:
      - trigger: target id
  - timer:
    - behaviour: every `period` seconds trigger target id
    - params
      - period: time in seconds
      - trigger: target id
